import FishPng from "../assets/fish.png";
import PigPng from "../assets/pig.png";

const copy = {
  en: {
    landing: {
      goingVegan: "Going Vegan 101",
      doingItTheRightWay: "Doing it the right way",
      kickstartYourJourney:
        "Kickstart your journey with our comprehensive guide!",
      letsGo: "Let's Go!"
    },
    enjoyedTheGuide:
      "Enjoyed the guide? <a href='#'>Download the abillionveg app</a> to keep your vegan journey going.",

    whatSection: {
      id: "what",
      longHeading: "Identities and the path to veganism",
      shortHeading: "What",
      subSections: [
        {
          id: "vegan-vs-plant-based",
          title: "Vegan vs Plant-based",
          body:
            'If you\'re unfamiliar with the specifics of meat-free dietary culture, the details can get a bit jumbled. People start tossing around terms like "vegetarian", "vegan", "soy-this" and "tofu-that" and before you know it you\'re lost. While vegetarians and vegans can technically be grouped together under the general umbrella of vegetarianism, you\'d be surprised that they are two very different diets. Let\'s take a deeper dive. <br /><br /> Veganism (as asserted by The Vegan Society) places equal emphasis on diet and overall lifestyle. Vegans abstain from all animal proteins, animal by-products (eggs, dairy, honey) and meat-by products (gelatin, animal broths, etc.) in their diet. Many within the vegan community are also strongly committed to the belief that veganism is more than just a diet and strive to forgo all products or possessions in their lifestyle either tested on animals or containing animal by-products (cosmetics, toiletries, clothing, etc.). Vegans are also sometimes noted for carrying stronger political and personal beliefs in regards to meat consumption and animal rights. <br /><br /> A plant-based diet, on the other hand, emphasises on eating whole, unrefined plant foods and staying away from or minimising meat, dairy products and highly processed foods for health reasons. That means that even vegan desserts made with refined sugar or bleached flour are out. Plant-based individuals do not restrict their purchase of leather goods. It\'s flexible in that way but strict in its focus on eating whole foods. <br /><br /> Many people adopt a plant-based diet and after experiencing dramatic health benefits, become interested in other reasons for avoiding animal products, such as environments and ethical issues surrounding factory farming. Some vegans who have already given up animal products for ethical reasons have decided to adopt a vegan plant-based diet after some time, for their health.'
        },
        {
          id: "vegetarian",
          title: "Vegetarian",
          body:
            "People are vegetarians for many reasons, including health, religious convictions, concerns about animal welfare, or a desire to eat in a way that avoids excessive use of environmental resources. <br /><br /> When people think about a vegetarian diet, they typically think about a diet that doesn't include meat, poultry or fish. Even within this categories, there are different levels and approaches to vegetarian diets. Let's take a glance at a couple quick guides to break down who eats what.  <br /><br /> Lacto-vegetarian diets exclude meat, fish, poultry and eggs as well as foods that contain them. Dairy products, like milk, cheese, yoghurt and butter, are included. <br /><br /> Ovo-vegetarian diets exclude meat, poultry, seafood and dairy products, but allow eggs. <br /><br /> Lacto-ovo vegetarian diets exclude meat, fish, and poultry, but allow dairy products and eggs."
        },
        {
          id: "pescatarian",
          title: "Pescatarian",
          body:
            "A pescatarian is someone who chooses to eat a vegetarian diet, but who also eats fish and other seafood, as well dairy and eggs."
        },
        {
          id: "flexitarian",
          title: "Flexitarian",
          body:
            "Some people follow a semi-vegetarian diet, also called a flexitarian diet, which is primarily a plant-based diet but includes meat, dairy, eggs, poultry and fish on occasion or in small quantities. "
        },
        {
          id: "omnivore",
          title: "Omnivore",
          body: "An omnivore is someone who eats animal- and plant-based foods."
        }
      ]
    },
    whySection: {
      id: "why",
      longHeading: "Why choose veganism",
      shortHeading: "Why",
      subSections: [
        {
          id: "animals",
          title: "Animals",
          topics: [
            {
              id: "meat",
              title: "Meat",
              whatsWrong: {
                title: "What’s wrong with meat?",
                blurb:
                  "Farmed animals are way more intelligent and sophisticated than we typically give them credit! Here are some fun facts about the species of farmed animals who are most commonly used for meat:"
              },
              whatsTheStory: {
                title: "So, what's the story with meat?",
                blurb:
                  "It's no surprise that a lot of people do not like the meat industry. With most discussions about the horrors of industrial livestock farming- the pollution, the waste, the welfare of animals and our health- it's hard not to feel a twinge of guilt and conclude that we should eat less meat. Yet most of us probably won't. Instead, we will mumble something about meat being tasty, that 'everyone' eats it, and that we only buy 'grass-fed' beef. Perhaps meat is tasty, but there is so much delectable vegetarian and vegan food that tastes just as great! (Check out the reviews on abillionveg!)"
              },
              whatsWrongImages: [
                {
                  imagePosition: "left",
                  imageSrc: FishPng,
                  caption:
                    "Fishes like to play games and even be petted, pass down cultural knowledge to the next generation, have memories that match or exceed other vertebrates and experience pain and fear just like mammals and other animals."
                },
                {
                  imagePosition: "right",
                  imageSrc: PigPng,
                  caption:
                    "Pigs are said to be more intelligent than three-year-old humans, can learn to play video games and enjoy toys, use a wide range of oinks, grunts, and squeals to communicate, and will outsmart larger pigs to gain a temporary food advantage."
                }
              ],

              furtherReading: {
                title: "Further Reading",
                links: [
                  {
                    title: "Farmed Animal Fundamentals",
                    url: "https://faunalytics.org/fundamentals-farmed-animals/"
                  }
                ]
              }
            },
            {
              id: "dairy",
              title: "Dairy",
              whatsWrong: {
                title: "What's wrong with dairy?",
                blurb:
                  "Cows are <strong><i>compassionate, social animals</strong></i> who lick each other to help remain calm in stressful situations, maintain complex social structures with <strong><i>strong familial bonds</strong></i>, form “grooming partnerships” similar to chimpanzees, and love to jump and play, especially in the sunshine. Cows can be dedicated mothers –– throughout life, mother and child maintain social contact and regularly enjoy each other’s companionship."
              },
              whatsTheStory: {
                title: "So, what's the story with dairy?",
                blurb:
                  "Cows produce milk for the same reason that humans do, to nourish their young. In order to force them to continue producing more milk, their reproductive systems are exploited through artificial insemination, milking regiments and sometimes drugs are used. Male calves are destined to end up in cramped veal crates where they will be fattened for beef, and females are sentences to the same sad fate as their mothers. A cow's natural lifespan is about 20 years, but cows used by the dairy industry are typically killed after five years because their bodies wear out from constantly being pregnant or lactating."
              },
              furtherReading: {
                title: "Further Reading",
                links: [
                  {
                    title: "dairytruth.org",
                    url: "https://dairytruth.org/"
                  },
                  {
                    title: "dairy-truth.com",
                    url: "https://www.dairy-truth.com/"
                  }
                ]
              }
            },
            {
              id: "eggs",
              title: "Eggs",
              whatsWrong: {
                title: "What's wrong with eggs?",
                blurb:
                  "Chickens are capable of having strong individual personalities, forming lasting friendships, empathy and recognizing stress in other chickens, demonstrating more intelligence than dogs and cats on some tests, anticipating the future and adjusting their behavior accordingly. Hens can be dedicated mothers –– mother hens defend their young from predators and begin to teach calls to her chicks before they even hatch."
              },
              whatsTheStory: {
                title: "So, what's the story with eggs?",
                blurb:
                  "Eggs, sadly, come from chickens who suffer the same treatment as cows. Since males chicks are worthless to the egg industry, they are disposed of like trash. Female chicks are sent to egg farms, where, due to decades of genetic manipulation and selective breeding, they produce 250-300 eggs per year. In nature, wild hens lay only 10 to 15 eggs annually. Like all birds, they lay eggs only during the breeding season and only for the purpose of reproducing — this unnaturally high rate of egg-laying results in disease and mortality. In a natural environment, chickens can live 10 to 15 years, but chicken bred for egg-laying are killed at just 12 to 18 months of age when their egg production declines. "
              },
              whatsTheStoryImages: [
                {
                  imagePosition: "left",
                  caption:
                    "Make a difference! Earn $1 for each plant-based dish and product you review and donate to organizations that rescue animals like Violet!"
                }
              ],

              furtherReading: {
                title: "Further Reading",
                links: [
                  {
                    title: "considertheegg.org",
                    url: "https://www.considertheegg.org/"
                  },
                  {
                    title: "egg-truth.com",
                    url: "https://www.egg-truth.com/"
                  }
                ]
              }
            },
            {
              id: "honey",
              title: "Honey",
              whatsWrong: {
                title: "What's wrong with honey?",
                blurb:
                  "Bees are <strong><i>way</i></strong> more intelligent than we typically give them credit! Forager bees <strong><i>communicate</i></strong> to the other bees in their hive by performing a dance to convey the direction and distance of a food source they found. Their dance even accounts for the movement of the sun, which we (humans) need <strong><i>vector calculus</i></strong> to account for."
              },
              whatsTheStory: {
                title: "So, what's the story with honey?",
                blurb:
                  "When farmers remove honey from a hive, they replace it with a sugar substitute which is detrimental to the bee's health since it lacks the essential micro-nutrients of honey. The honey industry, like many other commercial industries, is profit-driven where the welfare of the bees is often secondary to commercial gain. Queen bees often have their wings clipped by beekeepers to prevent them from leaving the hive to produce a new colony elsewhere, which would decrease productivity and lessen profit. Unlike bees, humans can thrive without honey in their diets. Luckily, there are a variety of readily-available vegan alternatives for those with a sweet tooth. Head to abillionveg to explore the many different options and find out where you can get them!"
              }
            },
            {
              id: "wool",
              title: "Wool",
              whatsWrong: {
                title: "What's wrong with wool?",
                blurb:
                  "Perhaps surprisingly, sheep have vibrant personalities. <i>“Sheep are actually surprisingly intelligent, with impressive memory and recognition skills. They build friendships, stick up for one another in fights, and feel sad when their friends are sent to slaughter”</i> (the BBC)."
              },
              whatsTheStory: {
                title: "So, what's the story with wool?",
                blurb:
                  "Breeding for continuous wool is when the trouble begins, and there's nothing stylish about that. Hundred of lambs die before the age of eight weeks from exposure or starvation, and mature sheep die from diseases, lack of shelter, and neglect. These harsh conditions are due to shearers who are usually paid by volume, not by the hour, and therefore work fast and violent to get their fleece as quickly as possible. Without human interference, sheep grow just enough wool to protect themselves from extreme temperatures, both heat and cold. If you're a vegetarian but wear wool, you should know that the sheep used to make your sweater are still ending up on someone's plate. The good news is, there are plenty of vegan 'wool' options that are warm to choose from, including bamboo, TENCEL, hemp and more. With so many ethical fabrics out there to choose from, why wear so much bad karma? "
              }
            },
            {
              id: "humane-meat",
              title: '"Humane" meat',
              whatsWrong: {
                title: 'What about grass-fed, free-range, "humane" meat?',
                blurb:
                  "Consumers would need to pay for direct costs such as more space per animal, an army of veterinarians and medical supplies for sick animals, and a reversion of the artificial breeding that has made animals grow meat and produce milk and eggs at ultra-fast rates. That level of welfare doesn't exist at the very best farms today, so even the steep price tag of the free-range eggs is still too low to guarantee that the animals have good lives. Male babies of egg-laying hens and male dairy cattle are still killed. Requiring larger cages or pens for the animals to have enough room to fly or walk around will require more space, meaning more deforestation than factory farms already require. The animals will require just as much food and water, if not more. 9 billion land animals are killed for human consumption every year in the U.S. Giving 9 billion animals enough land to roam would be an environmental disaster. <br /><br />If we want to bring about true change, the most humane first step we can take is to choose plants and not animals when we eat. "
              },
              whatsWrongQuotes: [
                "“Applied to meat, eggs, dairy, and every now and then even fish, the free-range label … should provide no more peace of mind than ‘all-natural,’ ‘fresh,’ or ‘magical’ … I could keep a flock of hens under my sink and call them free-range” — Jonathan Safran Foer, Eating Animals"
              ],
              whatsWrongImages: [
                {
                  imagePosition: "left",
                  caption:
                    "Make a difference! Earn $1 for each plant-based dish and product you review and donate to organizations that rescue animals like Marin!"
                }
              ]
            },
            {
              id: "getting-involved",
              title: "Getting Involved",
              paras: {
                title: "Getting Involved: Activism",
                content:
                  "If you are already vegan (or even if you are not) you might be looking for ways to push yourself further. You might feel like going vegan is just the starting point –– you can and should feel <strong><i>empowered</i></strong> to change the world so that, someday, it will be a safe and happy place for all animals. The best way to help animals is to be <strong><i>active</i></strong>. <br /><br /><strong><i>Being active</i></strong> means different things to different people, and different ways of being active fall within different people's comfort zones. <br /><br />Stages of Activism:<br /><ul><li>Beginner (An animal ally): <ul><li>Sign Petitions! <ul><li>Adding your signature to a petition is the simplest way to amplify your own voice and the voices of the animals!</li></ul><li>Use the abillionveg app! <ul><li>This is a fun and simple way for you to make an impact! abillionveg is a platform for you to voice your opinion about anything plant-based or cruelty-free. We share your feedback with business owners to help them shift to more plant-based options, and we support animal rescue work around the world every time you review.</li></ul></li></li></ul></li><li>Intermediate (find another term here):<ul><li>Volunteer at a sanctuary!<ul><li>Volunteering at sanctuaries is both impactful and an immensely rewarding experience! You can form deeper, more meaningful connections with farmed animals who were rescued and strengthen your commitment to the animal rights cause. You will be surprised not only by how cute and cuddly these animals can be, but also by how playful, intelligent, and relatable! You also make a difference in their lives by working to keep the sanctuary running and thriving!</li></ul></li></ul></li><li>Advanced (Animal Warrior):<ul><li>Volunteer with an animal advocacy organization!<ul><li>This is a great way to make a broader impact in the animal rights and sustainability movements.</li><li>Help our partners!<ul><li><a href='https://mercyforanimals.org/action-center' target='_blank'>Mercy for Animals</a></li><li><a href='https://www.ciwf.com/'>Compassion in World Farming</a></li><li><a href='https://www.seashepherdglobal.org/'>Sea Shepherd</a></li></ul></li><li>Try some street activism!<ul><li>Get involved with a grassroots organization such as the Save Movement, Anonymous for the Voiceless, and/or Direct Action Everywhere! These groups try to have a direct impact on people who have never thought about animal rights issues before and help shape public opinion.</li></ul></li></ul></li></ul></li></ul>"
              }
            }
          ]
        },
        {
          id: "environment",
          title: "Environment",
          topics: [
            {
              id: "resource-usage",
              title: "Resource Usage",
              paras: {
                title: "Resource Usage",
                content:
                  'We could feed so many humans with the resources we set aside to grow food for these animals –– the U.S alone could comfortably feed 800 million people with the resources it uses to produce the grain it saves for farmed animals. <br /><br /> In September 2018, the United Nations Environment Programme (UNEP) named meat "the world\'s most urgent problem" and stating that "Our use of animals as a food-production technology has bought us to the verge of catastrophe."<br /><br />Beef production has more than doubled since the 60s, whilst the production of chicken meat has increased by a factor of nearly 10, according to the Food and Agriculture Organisation of the United Nations (FAO). The bodies of chicken, beef, and eggs are bigger than ever, thanks to breeding tactics. Cow\'s milk production per animal has increased by 30% as has chicken egg production. Significant amounts of natural resources are required to keep these industries running.<br /><br />Eating a plant-baed diet is a win-win situation for our our health and Earth\'s health. Shifting away from animal-based foods could not only add up to 49% to the global food supply without expanding croplands but would so significantly reduce carbon emissions and waste byproducts that end up in our oceans. If each and every person in the US gave up meat and dairy products one or more days of the week, we would save the environment from thousands of tons of carbon emissions. In fact, in one year, animal farming creates as much carbon emissions as the entire transport sector. Similarly, by reducing our animal-based foods consumption, we would reduce our water use by half as animal farming utilises more than 50% of fresh water. Meatless Monday needs to be a daily thing!'
              }
            },
            {
              id: "land-usage",
              title: "Land Usage",
              paras: {
                title: "Land Usage",
                content:
                  "New research shows that without meat and dairy consumption, global farmland use could be reduced by more than 75% -an area equivalent to the US, China, European Union and Australia combined- and still feed the world. Being vegan minimises land usage and uses far fewer resources. Time to swap Hot Dogs for Soy."
              }
            },
            {
              id: "greenhouse-gases",
              title: "Greenhouse Gases",
              paras: {
                title: "Greenhouse Gases",
                content:
                  'Oxford University researchers concluded that going vegan is the most influential thing one can do to fight climate change. Lead researcher Joseph Poore said to the Guardian that adopting a vegan diet is "the single biggest way to reduce your impact on planet Earth, not just greenhouse gases, but global acidification, eutrophication, land use and water use." Poore himself became vegan after his first year of research. By going vegan, you massively lower your carbon footprint — even more than you would do by avoiding driving and flying.'
              }
            },
            {
              id: "ocean-dead-zones",
              title: "Ocean Dead zones",
              paras: {
                title: "Ocean Dead Zones",
                content:
                  "Our oceans are struggling. The ocean's declining health - as well as the consumption of seafood - is depleting fish stocks at a rapid rate. Some experts have said that the world's oceans could be empty of fish by 2048. More people are avoiding single-use plastics like straws due to their longevity and the consequent impact on animals and the oceans. However, half of the plastic found in the ocean comes from fishing nets, not straws. 70-90% of freshwater pollution in western countries is linked to animal agriculture, as pointed out on Cowspiracy's website. Runoff from factory farms and livestock grazing is a major contributor to the pollution of rivers and lakes. Simply by going vegan, you stop the rivers and lakes from pollution, hello clean beaches! A rising number of vegan seafood options - including everything from vegan crab and shrimp to fillets and tuna - you won't be missing out on that sushi you love without marine life or their habitats."
              }
            },
            {
              id: "deforestation",
              title: "Deforestation",
              paras: {
                title: "Deforestation",
                content:
                  "Research published by The American Journal of Clinical Nutrition outlined that a meat-eater’s diet requires 17 times more land than a vegetarian’s. The Oxford University analysis of farming found that 80% of the planet’s total farmland is used to rear livestock. Beef production requires 36 times more land than the production of plant-based protein like peas. The researchers stated that if everyone went vegan, global farmland use would drop by 75%. This decline would free up landmass the size of Australia, China, the EU, and the U.S. combined. Being vegans means being part of the change in the fight against climate change. "
              },
              paraQuotes: [
                "Myth Busted: Some say that vegans cause more rainforest destruction than non-vegans due to buying soy products, but 75% of the world's soy is set aside for animal feed — so the best way to reduce your global soy consumption is actually to go vegan!"
              ]
            },
            {
              id: "species-extinction",
              title: "Species Extinction",
              paras: {
                title: "Species Extinction",
                content:
                  "It's not just the animals we eat that suffer at the hands of meat, dairy, and egg industries. Wild animal populations are struggling more than ever before. The World Wildlife Foundation (WWF) said in its Appetite for Destruction summary report that humankind's appetite for meat places an \"enormous strain on our natural resources and is a driving force behind wide-scale biodiversity loss,\" especially thanks to the production of feed crops for livestock. The report claims that animal product consumption is the reason behind 60% of all biodiversity loss. <br /><br />In the fishing industry, billions of marine animals including endangered fish, whales, sea turtles, dolphins, and seals are caught and killed unintentionally. These animals are known as bycatch and they make up 40 percent of the world’s total catch, according to a report released by Oceana in March 2014.<br /><br />Proceedings of the National Academy of Sciences (PNAS) research estimates that wild animals make up just 4% of life on Earth, with livestock accounting for 60% of all life. Ditching animal products to go vegan could give wild animal populations the chance to rebalance and their habitats the opportunity to thrive."
              }
            },
            {
              id: "getting-involved",
              title: "Getting Involved",
              paras: {
                title: "Getting Involved: Sustainability",
                content:
                  "Straws <br /><br />BYO - bowl, cup, bottle, cutleries "
              }
            }
          ]
        },
        {
          id: "health",
          title: "Health",
          topics: [
            {
              id: "protein",
              title: "Protein",
              paras: {
                title: "What about protein?",
                content:
                  "Eating protein doesn't have to mean eating meat. The good news is that vegans hardly have to worry about protein! The Cornell China Study, indicates that even the average vegan consumes 70% more protein than they need each day. Our bodies already produce a sufficient amount of carnitine and creatine, which are the essential amino acids found in animal flesh, so we do not need to obtain those amino acids from external sources. Good vegan sources of protein include: whole grains, nuts, seeds, and legumes."
              }
            },
            {
              id: "calcium",
              title: "Calcium",
              paras: {
                title: "What about calcium?",
                content:
                  "It is possible to get enough calcium from a vegan diet. According to The Physicians Committee for Responsible Medicine (PCRM) - made up of 150,000 medical professionals, the healthiest source of calcium is not actually milk, but dark leafy greens and legumes. Good vegan sources of calcium include dried herbs, sesame seeds, figs, tofu, almonds, flax seeds, Brazil nuts, and kale. Most vegan milks (such as soy, almond, oat, etc.) are also fortified with calcium!"
              },
              paraQuotes: [
                "Fun fact: Calcium is a metal that comes from the earth. The calcium in cow’s milk originates from the grass, soy, or other crops used to feed the cows."
              ]
            },
            {
              id: "iron",
              title: "Iron",
              paras: {
                title: "What about iron?",
                content:
                  "Good vegan sources of iron include nuts, beans, and dark, leafy greens. Contrary to popular belief, there is no correlation between veganism and anemia. While many may believe that meat and seafood are the only way to get iron, beans, legumes, grains, nuts and seeds, dried fruits, leafy greens are some of the most iron-rich vegan foods, according to experts. "
              },
              paraQuotes: [
                "Fun fact: Vitamin C is an essential nutrient for effective iron absorption. Vegans generally have a better vitamin C intake than omnivores!"
              ]
            },
            {
              id: "b12",
              title: "B12",
              paras: {
                title: "What about B12?",
                content:
                  "Anyone who doesn't eat a balanced diet or supplement- not just vegans- are at risk of B12 deficiency. Many soy milks, meat alternatives, nutritional yeast, and other fortified foods have B12 in them to maintain good health. "
              },
              paraQuotes: [
                "Fun fact: Contrary to popular belief, vitamin B12 is not made by animals –– tiny microbes make B12, but the chlorine that is now used to filter our water supply kills off these microbes. The most hygienic, safe, and healthy way to obtain B12 is through supplements or fortified foods."
              ]
            },
            {
              id: "omega-3",
              title: "Omega-3",
              paras: {
                title: "What about omega-3?",
                content:
                  "It is possible that in taking the fatty acid from natural, plant-based sources like seeds and nuts, it could help to improve health. There are plenty of excellent vegan sources of omega-3! They include flax seeds, hemp seeds, edamame, wild rice, canola oil, walnuts, black beans, and kidney beans. As the world population increases, sustainability is a mounting concern. To preserve our ocean’s wildlife, flax may be the new fish in regard to sourcing our essential omega-3s."
              }
            },
            {
              id: "vitamin-d",
              title: "Vitamin D",
              paras: {
                title: "What about vitamin D?",
                content:
                  "Dr. Michael Greger (physician, New York Times bestselling author, and internationally recognized speaker on nutrition, food safety, and public health issues) recommends a vitamin D intake of 2,000 IU per day for each person, either from synthesizing it from sunlight or taking a supplement. If you manage your meals and lifestyle well, it is perfectly possible to get just the right amount of vitamin D from a vegan diet. <br /><br />We have difficulty obtaining sufficient vitamin D because we evolved in sunnier climates without winter seasons, and D3 is easier to get at lower latitudes. This isn't exclusive to vegans. For meat-eaters too, its hard to get enough vitamin D, and this is why plant-based vitamin D supplements are the way forward. Some vegan sources of vitamin D include mushroom, fortified plant-based milk (Soy, Almond, Coconut) and tofu. "
              },
              paraQuotes: [
                "Fun fact: Vitamin D3 is not actually a vitamin, but a hormone — vegan vitamin D3 supplements source D3 from lichen."
              ]
            },
            {
              id: "sports",
              title: "Sports",
              paras: {
                title: "Can I be an athlete on a vegan diet?",
                content:
                  "Yes! There are many vegan world-class athletes, including Venus Williams, Novak Djokovic, Tony Gonzales, Rich Roll, Ricky Williams, David Haye, Laura Dennis, Mac Danzig, Fiona Oakes, Steph Davis, and Patrik Baboumian. Athletes eating vegan report higher energy levels, quicker recovery times, better sleep, and fewer injuries. The key is not to worry about what you can't eat, but rather what you can eat, and what will be the best energy source for you. Here are some tips if you are an athlete and want to give veganism a try: <br /><br />1. &nbsp; &nbsp; Avoid processed foods! Stay away from added sugar, oil, and salt. Limiting these ingredients does not mean your diet will necessarily lack flavor and variety –– you can still use herbs, spices, and condiments! <br /><br />2. &nbsp; &nbsp; Make sure you consume <strong><i>enough calories</i></strong> to sustain your active lifestyle! Don't be afraid to eat <strong><i>lots of carbs</i></strong> –– not refined carbs, necessarily. There are plenty of nutritious carbohydrate-rich foods like quinoa, oats, buckwheat, sweet potatoes, beetroots, kidney beans, chickpeas, and many fruits.<br /><br />3. &nbsp; &nbsp; You do need to be more mindful of your protein intake than sedentary folks do. Protein helps you grow muscle! Some great vegan protein sources are seitan, soy products (such as tofu, tempeh, and edamame), lentils, beans, nutritional yeast, nuts, seeds, green peas, spirulina, and mock meats. It also might be great to try some vegan protein powder [ABV link needed]! <br /><br />4. &nbsp; &nbsp; Chow down on some vegan <strong>anti-inflammatory</strong> foods after your exercise routine! They will help heal inflammation and assist in preventing it in the future. <strong>TRY</strong> whole grains, leafy greens, blueberries, turmeric, sweet potatoes, nuts, seeds, soy, shiitake mushrooms, and garlic. <strong>AVOID</strong> sugar, sodium, artificial flavorings, and refined flours."
              }
            },
            {
              id: "pregnancy-and-small-children",
              title: "Pregnancy and Small Children",
              paras: {
                title:
                  "Can anyone (for example, pregnant women and small children) be vegan?",
                content:
                  "A vegan diet is suitable for individuals during all stages of the life cycle, including pregnancy, lactation, infancy, childhood, and adolescence. <br /><br /><strong>Here are some tips for if you are pregnant: </strong><br /><br />1. &nbsp; &nbsp; Iron: During pregnancy, you need to increase your iron intake, regardless of your dietary habits. It is also essential to consume foods that are high in vitamin C to help with iron absorption. <br /><br />2. &nbsp; &nbsp; B12: during pregnancy, you also ought to take a B12 supplement. Vegan cereals and nutritional yeast are also commonly fortified with it. B12 is essential for the formation of red blood cells and proper brain development.<br /><br />3. &nbsp; &nbsp; Folate: Expectant mothers are recommended to increase their folate intake. Tons of vegan foods — including chickpeas, beans, and green vegetables — are excellent sources of folate. Folate helps to prevent congenital disabilities relating to the brain and spine, such as spina bifida. <br /><br />4. &nbsp; &nbsp; Omega-3: These fatty acids are vital for your health, as well as your baby’s. <br /><br />5. &nbsp; &nbsp; Check out some vegan prenatal vitamins! [ABV link needed] <br /><br /><strong>Here are some tips for parents of small children:</strong><br /><br />1. &nbsp; &nbsp; Protein: Children can get all the protein that their bodies need from good vegan sources, like whole grains, nuts, seeds, and legumes.<br /><br />2. &nbsp; &nbsp; Iron: Some babies’ intestines bleed after drinking cow’s milk, increasing their risk of developing anemia. Formula-fed babies should be given a soy-based formula with added iron to minimize the risk of intestinal bleeding. Iron-rich foods such as raisins, almonds, dried apricots, blackstrap molasses, and fortified grain cereals will meet the needs of toddlers and children 12 months and older. Vitamin C helps the body absorb iron, so foods rich in both are particularly valuable. <br /><br /> 3. &nbsp; &nbsp; Calcium: Broccoli, kale, tofu, dried figs, tahini, great northern beans, blackstrap molasses, and fortified orange juice and soy milk are all excellent sources of calcium. As with iron, vitamin C will help your child’s system absorb calcium efficiently. <br /><br /> 4. &nbsp; &nbsp; Vitamin D: Vitamin D–enriched soy milk will provide this nutrient, but a child who spends as little as 10 to 15 minutes three times a week playing in the sunshine, with arms and face exposed, will also get sufficient vitamin D. <br /><br /> 5. &nbsp; &nbsp; Vitamin B12: B12 tablets will ensure an adequate amount of the vitamin for your child. Vitamin B12 is also found in fortified soy milk and many kinds of cereal. <br /><br /> 6. &nbsp; &nbsp; Check out some <strong>vegan children’s vitamins</strong>!<br /><br />"
              }
            },
            {
              id: "health-benefits",
              title: "Health Benefits",
              paras: {
                title:
                  "What are the health benefits that are linked to veganism?",
                content:
                  "<ul><li><strong><i>Higher nutrient intake</strong></i>: Nutrition levels will elevate naturally when a person consumes a diet rich in fresh vegetables, fruits, soy, nuts, whole grains. Their body reaps the benefits of increased nutrients and vitamins. Because all of these components come from the Earth uninterrupted with antibiotics or chemicals, they are loaded with vitamins and nutrients in potassium, magnesium, folate, and vitamins A, C and E. </li><li><strong><i>Weight loss</strong></i>: Nutrition levels will elevate naturally when a person consumes a diet rich in fresh vegetables, fruits, soy, nuts, whole grains. Their body reaps the benefits of increased nutrients and vitamins. Because all of these components come from the Earth uninterrupted with antibiotics or chemicals, they are loaded with vitamins and nutrients in potassium, magnesium, folate, and vitamins A, C and E. </li><li><strong><i>Lower blood sugar levels and healthier kidneys</strong></i>: Vegans tend to have lower blood sugar levels, higher insulin sensitivity, and a significantly lower risk of developing type 2 diabetes. Several studies report that a vegan diet may be able to relieve systemic distal polyneuropathy symptoms — a condition in people with diabetes that causes sharp, burning pain!</li><li><strong><i>Protection against certain cancers</i></strong>: According to the World Health Organization, about one-third of all cancers are preventable by factors within your control, including diet. Increasing consumption of fresh fruits, vegetables, and legumes while limiting the use of processed, smoked, and overcooked meat can help reduce cancer risk. These aspects of many plant-based diets may offer protection against prostate, breast, and colon cancers.</li><li><strong><i>Reduction of arthritic symptoms</i></strong>: Vegan diets based on probiotic-rich whole foods can significantly decrease symptoms of osteoarthritis and rheumatoid arthritis, such as pain, joint swelling, and morning stiffness.</li><li><strong><i>The Quality of Physicality Improves</strong></i>: Vegan diet health benefits in relation to a person's physical appearance undoubtedly surround the fact that such a way of life is calorie-friendly. Because of this, a person's body mass index (BMI) lowers, leading to weight loss. Lean statures, and the ability to be more physically fit due to an increase in en</li></ul>"
              }
            },
            {
              id: "getting-involved",
              title: "Getting Involved",
              paras: {
                title: "Getting Involved",
                content:
                  "organisations that work with schools, hospitals etc to introduce healthier plant-based options <br /><br />SAVE Health Movement"
              }
            }
          ]
        }
      ]
    },

    howSection: {
      id: "how",
      longHeading: "How to start a plant-based diet",
      shortHeading: "How",
      subSections: [
        {
          id: "transition",
          title: "Transition",
          topics: [
            {
              id: "edu-ma-cate-yourself",
              title: "Edu-ma-cate yourself!",
              paras: {
                title: "Edu-ma-cate yourself!",
                content:
                  "Find your motivation for going vegan! Learn the issues you contribute to by eating omnivorously and learn about the benefits of a vegan lifestyle. You can flip through articles and videos online, look for vegan influencers on social media, read books, and/or watch documentaries. Get yourself pumped to make a difference!  <br /><br /> Research how to eat happily and healthily as a vegan. Look up information regarding nutrition. <br /><br />Learn how to tell if a food or product is vegan! Check out ingredients lists. Figure out which questions to ask."
              },
              paraQuotes: [
                "\"Ingredients lists and allergen warnings are your best friends at the grocery store. Always look at the allergens first. That will help the process go faster. If it says 'contains milk or eggs,' put the package down. There's no need to check. If it says 'contains wheat only' you can read the rest of the ingredients\" — @sam0729"
              ]
            },
            {
              id: "keep-a-positive-attitude-and-an-open-mind",
              title: "Keep a positive attitude and an open mind!",
              paras: {
                title: "Keep a positive attitude and an open mind!",
                content:
                  'Remember that there is so much delicious vegan food out there you will expose yourself to! Many dishes can be "veganized," so you will not need to give up your old favorites. There are also a variety of traditionally vegan recipes from cuisines all over the world. Therefore, you might be surprised what your new favorite meals will be! Remember that being vegan only gets easier with time.'
              }
            },
            {
              id: "add-before-you-subtract",
              title: "Add before you subtract!",
              paras: {
                title: "Add before you subtract!",
                content:
                  "Incorporate more whole grains, beans, legumes, tofu, nuts, seeds, and vegetables into your diet before you go fully vegan. Find substitutes for your favorite meat, dairy, and egg products –– you'll be surprised how much is out there! Then, work on cutting down your animal product consumption.<br /><br /> Start collecting and experimenting with vegan recipes that appeal to you. Find vegan recipes you love and become more familiar with them. If you like to eat out, find some vegan options near you using our app!<br /><br /> Switch out dairy milk for a plant-based alternative such as soy, almond, oat, rice, or another. There are so many options, so experiment to find out which ones you like best! Different plant-based milks work best in different drinks, dishes, and contexts.<br /><br />"
              }
            },

            {
              id: "baby-steps",
              title: "Baby steps",
              paras: {
                title: "Baby steps",
                content:
                  "Some people find it easier to go vegetarian first, then cut out dairy, eggs, and other animal products either all at once or incrementally. But make sure you are reducing your overall consumption of animal products when if you go vegetarian first, rather than merely replacing the meat in your diet with dairy and eggs."
              }
            }
          ]
        },
        {
          id: "substitute",
          title: "Substitute",
          topics: [
            {
              id: "dairy-alternatives",
              title: "Dairy Alternatives",
              paras: {
                title:
                  "Check out some dairy alternatives on the abillionveg app! [ABV links needed]",
                content:
                  "<ul><li>Vegan milk<ul><li>Soy milk</li><li>Almond milk</li><li>Coconut milk</li><li>Oat milk</li><li>Rice milk</li><li>Cashew milk</li></ul></li><li>Vegan cheese</li><li>Vegan ice cream</li></ul>"
              }
            },
            {
              id: "egg-alternatives",
              title: "Egg Alternatives",
              paras: {
                title:
                  "Check out some egg alternatives on the abillionveg app! [ABV links needed]",
                content:
                  "<ul><li>Vegan eggs</li><li>Ground Flax</li><li>Chia Seeds</li><li>Soy Protein</li><li>Ripe banana</li><li>Unsweetened applesauce</li><li>Peanut butter</li></ul>"
              }
            },
            {
              id: "honey-alternatives",
              title: "Honey Alternatives",
              paras: {
                title:
                  "Check out some honey alternatives on the abillionveg app! [ABV links needed]",
                content:
                  "<ul><li>Agave</li><li>Barley malt syrup</li><li>Bee-free honey</li><li>Brown rice syrup</li><li>Coconut nectar</li><li>Maple syrup</li><li>Date Syrup</li><li>Molasses</li><li>Golden Syrup</li></ul>"
              }
            }
          ]
        },
        {
          id: "social-advice",
          title: "Social Advice",
          paras: {
            title: "Social Advice",
            content:
              '<ul><li><strong>Can you give me some advice about being vegan as someone who still lives with their parents or guardians?</strong><ul><li><i>People from all kinds of unlikely backgrounds go vegan, including those who come from cultures with meat-centric cuisine or are born into families that have raised farmed animals for generations! You can go vegan no matter what your background is.</i></li><li><i>“[I] try to buy my vegan products as it\'s hard to change [my parents’] mindset, especially my mother\'s, partially due to culture and personality differences … When the mood is right, I will suggest buying vegan house care products but phrase it tactfully that it\'s good for skin, on sale, etc.” (A).</i></li><li><i>"If your parents/grandparents still prepare meals for you, you can watch youtube videos (Gaz Oakley, Cheap Lazy vegan) on how to prepare appetising vegan food to impress the whole family or at least yourself" (@opheeeliaaa).</i></li><li><i>“Start cooking at home, maybe once a week. If [your parents or guardians] are unsure of what vegan food or snacks they can share with you, give them options or alternatives. In my house, my parents get the bread that is the freshest at the supermarket, but some of these brands [use] milk … So I told them which brand they should buy so all of us can share … If they find it hard to adapt to your lifestyle change, have patience and be respectful” (Dash).</i></li><li><i>"Show your loved ones a short video, like <a href="https://www.youtube.com/watch?v=baffBE-1Lg4&feature=youtu.be" target="_blank">this one</a>, to help them empathize with your commitment to veganism. It can be hard to explain this in your own words to them, especially since interactions with loved ones can easily become quite emotionally charged. Additionally, your loved ones are more likely to care about your feelings than broader topics such as animal rights, climate change, or resource conservation –– providing them with an explanation of why veganism is important to you (or your health) will probably gauge their sympathy more effectively than discussing these issues themselves" (Lily).</i></li></ul></li><li><strong>Can you give me some advice about being vegan during holidays and social gatherings?</strong></li><ul><li><i>Being vegan can be tougher during holidays and social gatherings because no one wants to feel like they are in the way. There can also be more nostalgia and pressure from family and loved ones to make exceptions in your diet, which may not be something you are comfortable with. Not to worry, though –– we have strategies for you!</i></li><li><i>"[My family members] … are pretty against vegetarianism as it contradicts the culture they grew up in, where meat is a luxury and people are ingrained to flaunt their wealth/social status to gain respect and get to places … So when they are going on about how ‘weak and skinny’ I look, or how ‘you can\'t get married because you\'re a vegetarian,’ I will listen and tell them ‘I\'m doing well, don\'t worry’"</i></li><li><i>“[For social gatherings], call up the host and let them know in advance about your [dietary] preferences. Offer to bring some of your own food!” (Dash)</i></li><li><i>“Eat a little something ahead or make plans for later so you can enjoy the party without thinking about food” (Vikas)</i></li><li><i>"Remember that you are not only making a statement, you are taking action toward a better world. Your food choices are impacting the environment and so many innocent animals who suffer daily. By choosing a vegan diet, you are saving animals, contributing to a healthier planet, and shaping a better you. You are more powerful than you know" (@sam0729).</i></li><li><strong>Use the abillionveg app!</strong></li></ul></ul>'
          }
        },
        {
          id: "staying-motivated",
          title: "Staying Motivated",
          paras: {
            title: "Staying Motivated",
            content:
              "<ul><li>Find a friend or a group that is also vegan or trying to go vegan. It helps when you have people to help you figure out how to transition!</li><li>Follow vegan social media influencers (like @deliciouslyella or @morethanveggies), companies, chefs, and activists because they can help fuel you with excitement about your transition to a vegan lifestyle. They can inspire you and show you how it's done!</li><li>Make vegan friends by joining vegan Facebook groups, attending vegan events in your community (like VegFests or potlucks), and getting involved in local activism. Having an understanding and supportive community of people is essential for your mental health and motivation!</li><li>- Make vegan friends by joining vegan Facebook groups, attending vegan events in your community (like VegFests or potlucks), and getting involved in local activism. Having an understanding and supportive community of people is essential for your mental health and motivation!</li><li>Visit animal sanctuaries and personally interact with former victims of animal agriculture. It can be a heartwarming experience and strengthen your commitment to veganism.</li><li>Make a list of reasons why you are motivated to go vegan. It helps to refer to this list when you go through situations or periods when you feel less motivated. </li></ul>"
          },
          paraQuotes: [
            "“Go to a vegan event or festival. One of the best ways to get excited and motivated about being vegan is spending time around other people who are too! Not all cities have good vegan events, but they're growing, they're tons of fun and maybe you'll be the first to start one in your area one day!” (@tryingveganwithmario).",
            '"Check out Pinterest, Google, and YouTube! There are thousands of delicious vegan recipes out there! I have never experimented with food this much until I became vegan. Plant-based foods have limitless potential! Who knew cashews could be used to make ice-cream, cheesecake, or cheese?! Have faith and take the leap" (@sam0729).',
            "“Put yourself in the shoes of the animals. Imagine how you would feel” (@georgejacobs)",
            'Remember: "Learn to forgive yourself when you can’t be vegan for that day! Veganism is not about perfection but about how you strive to improve yourself! You learn along the way" (@opheleeliaaa).'
          ]
        }
      ]
    }
  }
};

export default copy;
