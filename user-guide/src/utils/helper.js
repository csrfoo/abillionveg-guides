import React from "react";
import theme from "../globalStyles.js";

export function getSectionColor(currSection, copy) {
  let color;
  switch (currSection) {
    case copy.whatSection.id:
      color = theme.colorTurqoise;
      break;
    case copy.whySection.id:
      color = theme.colorPurple;
      break;
    case copy.howSection.id:
      color = theme.colorGreen;
      break;
    default:
      color = theme.colorBlack;
  }
  return color;
}

export function addLineBreaks(string) {
  return string.split("\n").map((text, index) => (
    <p key={`${text}-${index}`}>
      {text}
      <br />
    </p>
  ));
}

export function createMarkup(string) {
  return { __html: `${string}` };
}

export function scrollBy(pageHeight, callback) {
  const onScroll = function() {
    if (window.pageYOffset + 50 === pageHeight) {
      window.removeEventListener("scroll", onScroll);
      callback();
    }
  };
  window.addEventListener("scroll", onScroll);
  window.scrollBy({
    top: pageHeight - 50,
    behavior: "smooth"
  });
}

export function getSubSections(copy) {
  let resultWhat = copy.whatSection.subSections
    .map(item => `${item.id}`)
    .join("|");
  let resultWhy = copy.whySection.subSections
    .map(item => `${item.id}`)
    .join("|");
  let resultHow = copy.howSection.subSections
    .map(item => `${item.id}`)
    .join("|");
  return resultWhat.concat("|", resultWhy, "|", resultHow);
}

export function getWhatSubSections(copy) {
  let result = copy.whatSection.subSections.map(item => `${item.id}`).join("|");
  return result;
}

export function getWhySubSections(copy) {
  let result = copy.whySection.subSections.map(item => `${item.id}`).join("|");
  return result;
}
export function getHowSubSections(copy) {
  let result = copy.howSection.subSections.map(item => `${item.id}`).join("|");
  return result;
}

export function getTopics(copy) {
  let resultWhat = "";
  let resultWhy = copy.whySection.subSections.map(item =>
    item.topics.map(innerItem => `${innerItem.id}`).join("|")
  );
  let resultHow = copy.howSection.subSections.map(
    item =>
      !!item.topics
        ? item.topics.map(innerItem => `${innerItem.id}`).join("|")
        : undefined
  );

  return resultWhat.concat(resultWhy, "|", resultHow);
}

export function getWhyTopics(copy) {
  let result = copy.whySection.subSections.map(item =>
    item.topics.map(innerItem => `${innerItem.id}`).join("|")
  );
  return result.join("|");
}
export function getHowTopics(copy) {
  let result = copy.howSection.subSections.map(
    item =>
      !!item.topics
        ? item.topics.map(innerItem => `${innerItem.id}`).join("|")
        : undefined
  );
  return result.join("|");
}
