import React, { Component } from "react";
import PropTypes from "prop-types";

import BreadcrumbsContainer from "./BreadcrumbsContainer.js";

class Breadcrumbs extends Component {
  static propTypes = {
    crumbs: PropTypes.array.isRequired,
    sectionColor: PropTypes.string.isRequired
  };

  render() {
    const { crumbs, sectionColor } = this.props;
    return <BreadcrumbsContainer crumbs={crumbs} sectionColor={sectionColor} />;
  }
}

export default Breadcrumbs;
