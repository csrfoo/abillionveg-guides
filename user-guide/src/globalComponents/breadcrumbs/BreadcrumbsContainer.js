import React, { Component } from "react";
import styled from "styled-components";
import { withRouter } from "react-router-dom";

class BreadcrumbsContainer extends Component {
  handleBreadcrumbClick(crumb, index) {
    const { crumbs } = this.props;
    if (index !== crumbs.length - 1) {
      this.props.history.push(crumb.url);
    }
  }
  render() {
    const { crumbs, sectionColor } = this.props;
    return (
      <Container>
        {crumbs.map((crumb, index) => (
          <P
            onClick={() => this.handleBreadcrumbClick(crumb, index)}
            key={index}
            isLast={index === crumbs.length - 1}
            sectionColor={sectionColor}
          >
            {crumb.title} <Span isVisible={index < crumbs.length - 1}> / </Span>
          </P>
        ))}
      </Container>
    );
  }
}

export default withRouter(BreadcrumbsContainer);

const Container = styled.div`
  margin-bottom: 1em;
  margin-top: 1em;
`;

const P = styled.p`
  cursor: ${props => (props.isLast ? "initial" : "pointer")}
  font-size: 16px;
  display: inline-block;
  margin: 0
  color: ${props =>
    props.isLast ? props.sectionColor : props.theme.colorLightGrey};
`;

const Span = styled.span`
  padding: 1em;
  display: ${props => (props.isVisible ? "inline" : "none")};
`;
