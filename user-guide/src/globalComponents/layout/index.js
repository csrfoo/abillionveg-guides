import React, { Component } from "react";
import PropTypes from "prop-types";

import LayoutContainer from "./LayoutContainer.js";

class Layout extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  render() {
    const { copy } = this.props;
    return <LayoutContainer copy={copy} children={this.props.children} />;
  }
}

export default Layout;
