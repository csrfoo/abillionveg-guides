import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { withRouter } from "react-router-dom";

import theme from "../../globalStyles.js";
import { getSectionColor } from "../../utils/helper.js";

import DesktopNav from "../desktopNav";
import Header from "../../views/mainBody/header";

import BackgroundPng from "../../assets/background.png";

class LayoutContainer extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.container = React.createRef();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.location.pathname !== this.props.location.pathname) {
      if (window.innerWidth < theme.mobileDesktopDesignBreakpoint) {
        window.scrollTo(
          0,
          !!this.container &&
            !!this.container.current &&
            this.container.current.offsetTop - theme.verticalOffsetMobile
        );
      }
    }
  }
  render() {
    const { copy, children } = this.props;
    const borderColor = getSectionColor(
      this.props.match.params.sectionId,
      copy
    );

    return (
      <Container borderColor={borderColor} ref={this.container}>
        <Header copy={copy} />
        <AppBodyContent>
          <DesktopNavContainer>
            <DesktopNav copy={copy} />
          </DesktopNavContainer>
          {children}
        </AppBodyContent>
      </Container>
    );
  }
}

export default withRouter(LayoutContainer);

const Container = styled.div`
  border: 10px solid ${props => props.borderColor};
  min-height: 100vh;
  background-image: url(${BackgroundPng});
  margin-top: 100px;
  padding: 1em
  @media(min-width: 600px){
    padding: 1em 2em
  }
  @media (min-width: 800px) {
    margin-top: 0;
    min-height: 0
    height: calc(100vh - 50px);
        box-sizing: border-box;
  }
  @media (min-width: 1200px){
    padding: 2em 6em
  }
`;

const AppBodyContent = styled.div`
  display: flex;
  @media (min-width: 800px) {
    height: 70%;
  }
  @media (min-width: 1200px) {
    height: 80%;
    padding-bottom: 1em;
    overflow: hidden;
    max-width: 1200px;
    margin: 0 auto;
  }
`;

const DesktopNavContainer = styled.div`
  @media (min-width: 1200px) {
    overflow-y: scroll;
    flex: 1;
  }
`;
