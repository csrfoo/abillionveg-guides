import React, { Component } from "react";

import NotFound from "./NotFound.js";

class NotFoundContainer extends Component {
  render() {
    return <NotFound />;
  }
}

export default NotFoundContainer;
