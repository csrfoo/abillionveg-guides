import React, { Component } from "react";
import { withRouter } from "react-router-dom";

class NotFound extends Component {
  componentDidMount() {
    if (this.props.location.pathname !== "/") {
      window.location.pathname = "/";
    }
  }

  render() {
    return <div />;
  }
}

export default withRouter(NotFound);
