import React, { Component } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { withRouter, Link } from "react-router-dom";

import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton
} from "react-share";

import routes from "../../constants/routes.js";
import externalLinks from "../../constants/externalLinks.js";

import { getSectionColor } from "../../utils/helper.js";
import AccordionContainer from "../accordion/AccordionContainer.js";

class DesktopNavContainer extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      copy: props.copy,
      params: props.match.params,
      openSection: {},
      innerOpenSection: {},
      sectionColor: getSectionColor(
        this.props.match.params.sectionId,
        this.props.copy
      )
    };
  }

  componentDidMount() {
    const { sectionId } = this.props.match.params;
    const { subSectionId } = this.props.match.params;
    this.setState({
      openSection: { [sectionId]: true },
      innerOpenSection: { [subSectionId]: true }
    });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params !== this.props.match.params) {
      this.setState({
        sectionColor: getSectionColor(
          this.props.match.params.sectionId,
          this.props.copy
        ),
        params: this.props.match.params,
        openSection: { [this.props.match.params.sectionId]: true },
        innerOpenSection: { [this.props.match.params.subSectionId]: true }
      });
    }
  }

  handleOnClick(id) {
    const { openSection } = this.state;
    const isOpen = !!openSection[id];
    this.setState(prevState => ({
      openSection: { [id]: !isOpen },
      innerOpenSection: {}
    }));
  }

  handleInnerAccordionOnClick(id) {
    const { innerOpenSection } = this.state;
    const isOpen = !!innerOpenSection[id];
    this.setState(prevState => ({
      openSection: { ...prevState.openSection },
      innerOpenSection: {
        [id]: !isOpen
      }
    }));
  }

  renderWhatNav() {
    const { openSection, sectionColor, copy, params } = this.state;
    const { whatSection } = copy;

    return (
      <AccordionContainer
        onClick={id => this.handleOnClick(id)}
        isOpen={openSection[whatSection.id] === true}
        isMainAccordion={true}
        sectionColor={sectionColor}
      >
        <div label={whatSection.shortHeading} id={whatSection.id}>
          {whatSection.subSections.map((subSection, index) => (
            <div key={index}>
              <Link to={`/${whatSection.id}/${subSection.id}`}>
                <LinkText
                  isActive={params.subSectionId === subSection.id}
                  sectionColor={sectionColor}
                >
                  {subSection.title}
                </LinkText>
              </Link>
            </div>
          ))}
        </div>
      </AccordionContainer>
    );
  }

  renderWhyNav() {
    const {
      openSection,
      innerOpenSection,
      sectionColor,
      copy,
      params
    } = this.state;
    const { whySection } = copy;

    return (
      <AccordionContainer
        onClick={id => this.handleOnClick(id)}
        isOpen={openSection[whySection.id] === true}
        isMainAccordion={true}
        sectionColor={sectionColor}
      >
        <div label={whySection.shortHeading} id={whySection.id}>
          {whySection.subSections.map((subSection, index) => (
            <AccordionContainer
              key={index}
              onClick={id => this.handleInnerAccordionOnClick(id)}
              isOpen={innerOpenSection[subSection.id] === true}
              isMainAccordion={false}
              sectionColor={sectionColor}
              isInnerAccordionActive={params.subSectionId === subSection.id}
            >
              <div label={subSection.title} id={subSection.id}>
                {subSection.topics.map((topic, index) => (
                  <div key={index}>
                    <Link to={`/${whySection.id}/${subSection.id}/${topic.id}`}>
                      <LinkText
                        isActive={params.topicId === topic.id}
                        sectionColor={sectionColor}
                      >
                        {topic.title}
                      </LinkText>
                    </Link>
                  </div>
                ))}
              </div>
            </AccordionContainer>
          ))}
        </div>
      </AccordionContainer>
    );
  }

  renderHowNav() {
    const {
      openSection,
      innerOpenSection,
      sectionColor,
      copy,
      params
    } = this.state;
    const { howSection } = copy;

    return (
      <AccordionContainer
        onClick={label => this.handleOnClick(label)}
        isOpen={openSection[howSection.id] === true}
        isMainAccordion={true}
        sectionColor={sectionColor}
      >
        <div label={howSection.shortHeading} id={howSection.id}>
          {howSection.subSections.map(
            (subSection, index) =>
              !!subSection.topics ? (
                <AccordionContainer
                  key={index}
                  onClick={label => this.handleInnerAccordionOnClick(label)}
                  isOpen={innerOpenSection[subSection.id] === true}
                  isMainAccordion={false}
                  sectionColor={sectionColor}
                  isInnerAccordionActive={params.subSectionId === subSection.id}
                >
                  <div label={subSection.title} id={subSection.id}>
                    {subSection.topics.map((topic, index) => (
                      <div key={index}>
                        <Link
                          to={`/${howSection.id}/${subSection.id}/${topic.id}`}
                        >
                          <LinkText
                            isActive={params.topicId === topic.id}
                            sectionColor={sectionColor}
                          >
                            {topic.title}
                          </LinkText>
                        </Link>
                      </div>
                    ))}
                  </div>
                </AccordionContainer>
              ) : (
                <div key={index}>
                  <Link to={`/${howSection.id}/${subSection.id}`}>
                    <LinkText
                      isActive={params.subSectionId === subSection.id}
                      sectionColor={sectionColor}
                    >
                      {subSection.title}
                    </LinkText>
                  </Link>
                </div>
              )
          )}
        </div>
      </AccordionContainer>
    );
  }

  renderShare() {
    const socials = [
      {
        iconClass: "fab fa-2x fa-facebook-square",
        shareButton: FacebookShareButton
      },
      {
        iconClass: "fab fa-2x fa-linkedin",
        shareButton: LinkedinShareButton
      },
      {
        iconClass: "fab fa-2x fa-twitter-square",
        shareButton: TwitterShareButton
      }
    ];

    const { sectionColor } = this.state;

    return (
      <ShareContainer>
        {socials.map((social, index) => (
          <social.shareButton
            key={index}
            url={`${externalLinks.userGuideShareLink}${routes.home}`}
            {...social.additionalProps}
          >
            <SocialIconDiv sectionColor={sectionColor}>
              <i className={social.iconClass} />
            </SocialIconDiv>
          </social.shareButton>
        ))}
      </ShareContainer>
    );
  }
  render() {
    return (
      <Container>
        <ContainerNav>
          {this.renderWhatNav()}
          {this.renderWhyNav()}
          {this.renderHowNav()}
        </ContainerNav>
        <Divider />
        {this.renderShare()}
      </Container>
    );
  }
}

export default withRouter(DesktopNavContainer);

const Container = styled.div`
  display: none;
  flex: 0;
  @media (min-width: 800px) {
    display: block;
    flex: 2;
    margin-right: 3em;
    height: 100%;
    overflow: scroll;
  }
`;

const ContainerNav = styled.div`
  margin-bottom: 2em;
`;

const LinkText = styled.p`
  color: ${props =>
    props.isActive ? props.sectionColor : props.theme.colorLightGrey};
  font-weight: ${props => (props.isActive ? 600 : "initial")};
  &:hover {
    color: ${props => props.sectionColor};
  }
`;

const SocialIconDiv = styled.div`
  cursor: pointer;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  color: ${props => props.theme.colorGrey};
  &:hover {
    color: ${props => props.sectionColor};
  }
`;

const ShareContainer = styled.div`
  margin-top: 2em;
  margin-bottom: 2em;
  display: flex;
  width: 120px
  justify-content: space-between;
  div {
    flex: 1
    &:focus {
      outline-width: 0px !important;
      outline: none !important;
    }
  }
`;

const Divider = styled.div`
  height: 2px;
  background: ${props => props.theme.colorGrey};
`;
