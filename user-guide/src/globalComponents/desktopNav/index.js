import React, { Component } from "react";
import PropTypes from "prop-types";

import DesktopNavContainer from "./DesktopNavContainer.js";

class DesktopNav extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  render() {
    const { copy } = this.props;
    return <DesktopNavContainer copy={copy} />;
  }
}

export default DesktopNav;
