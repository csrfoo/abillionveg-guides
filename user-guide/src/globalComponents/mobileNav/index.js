import React, { Component } from "react";
// import PropTypes from "prop-types";

import MobileNavContainer from "./MobileNavContainer.js";

class MobileNav extends Component {
  static propTypes = {
    // copy: PropTypes.object.isRequired
  };

  render() {
    const { copy } = this.props;
    return <MobileNavContainer copy={copy} />;
  }
}

export default MobileNav;
