import React, { Component } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import _isEmpty from "lodash/isEmpty";

import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton
} from "react-share";

import { withRouter, Link } from "react-router-dom";

import routes from "../../constants/routes.js";
import externalLinks from "../../constants/externalLinks.js";

import SocialFacebookPng from "../../assets/social-facebook.png";
import SocialLinkedinPng from "../../assets/social-linkedin.png";
import SocialTwitterPng from "../../assets/social-twitter.png";
import SharePng from "../../assets/share.png";

import theme from "../../globalStyles.js";

class MobileNavContainer extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      openSection: {},
      innerOpenSection: {},
      showShareOptions: false
    };
  }

  UNSAFE_componentWillMount() {
    document.addEventListener("mousedown", this.handleOnShareClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleOnShareClick, false);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.location.pathname !== this.props.location.pathname) {
      this.setState({
        openSection: {}
      });
    }
  }

  handleNavItemClick(id) {
    const { openSection } = this.state;
    const isOpen = !!openSection[id];
    this.setState(prevState => ({
      openSection: { [id]: !isOpen }
    }));
  }

  handleOnSubSectionClick(id) {
    const { innerOpenSection } = this.state;
    const isOpen = !!innerOpenSection[id];
    this.setState(prevState => ({
      openSection: { ...prevState.openSection },
      innerOpenSection: {
        [id]: !isOpen
      }
    }));
  }

  renderNavMainSections(section, bgColor) {
    return (
      <NavItem
        onClick={() => this.handleNavItemClick(section.id)}
        bgColor={bgColor}
      >
        <p>{section.shortHeading}</p>
      </NavItem>
    );
  }

  renderWhatDropdown() {
    const { copy } = this.props;
    const { whatSection } = copy;
    const { openSection } = this.state;
    return (
      <Dropdown isVisible={openSection[whatSection.id] === true}>
        {whatSection.subSections.map((subSection, index) => (
          <Link to={`/${whatSection.id}/${subSection.id}`} key={index}>
            <SubSectionDiv key={index} bgColor={theme.colorTurqoise}>
              <SubSectionTitle>
                {subSection.title}
                <span>>></span>
              </SubSectionTitle>
            </SubSectionDiv>
          </Link>
        ))}
      </Dropdown>
    );
  }

  renderWhyDropdown() {
    const { copy } = this.props;
    const { whySection } = copy;
    const { openSection, innerOpenSection } = this.state;
    return (
      <Dropdown isVisible={openSection[whySection.id] === true}>
        {whySection.subSections.map((subSection, index) => (
          <SubSectionDiv
            key={index}
            bgColor={theme.colorPurple}
            onClick={() => this.handleOnSubSectionClick(subSection.id)}
          >
            <SubSectionTitle>
              {subSection.title}

              {innerOpenSection[subSection.id] === true ? (
                <span>-</span>
              ) : (
                <span>+</span>
              )}
            </SubSectionTitle>

            <TopicsDiv
              bgColor={theme.colorLightPurple}
              isVisible={innerOpenSection[subSection.id] === true}
            >
              {subSection.topics.map((topic, index) => (
                <Link
                  key={index}
                  to={`/${whySection.id}/${subSection.id}/${topic.id}`}
                >
                  <TopicTitle>
                    {topic.title}
                    <span>>></span>
                  </TopicTitle>
                </Link>
              ))}
            </TopicsDiv>
          </SubSectionDiv>
        ))}
      </Dropdown>
    );
  }

  renderHowDropdown() {
    const { copy } = this.props;
    const { howSection } = copy;
    const { openSection, innerOpenSection } = this.state;
    return (
      <Dropdown isVisible={openSection[howSection.id] === true}>
        {howSection.subSections.map(
          (subSection, index) =>
            !!subSection.topics ? (
              <SubSectionDiv
                key={index}
                bgColor={theme.colorGreen}
                onClick={() => this.handleOnSubSectionClick(subSection.id)}
              >
                <SubSectionTitle>
                  {subSection.title}

                  {innerOpenSection[subSection.id] === true ? (
                    <span>-</span>
                  ) : (
                    <span>+</span>
                  )}
                </SubSectionTitle>

                <TopicsDiv
                  bgColor={theme.colorLightGreen}
                  isVisible={innerOpenSection[subSection.id] === true}
                >
                  {subSection.topics.map((topic, index) => (
                    <Link
                      key={index}
                      to={`/${howSection.id}/${subSection.id}/${topic.id}`}
                    >
                      <TopicTitle>
                        {topic.title}
                        <span>>></span>
                      </TopicTitle>
                    </Link>
                  ))}
                </TopicsDiv>
              </SubSectionDiv>
            ) : (
              <Link to={`/${howSection.id}/${subSection.id}`} key={index}>
                <SubSectionDiv key={index} bgColor={theme.colorGreen}>
                  <SubSectionTitle>
                    {subSection.title}
                    <span>>></span>
                  </SubSectionTitle>
                </SubSectionDiv>
              </Link>
            )
        )}
      </Dropdown>
    );
  }

  handleOnShareClick = e => {
    if (this.shareIconDiv.contains(e.target)) {
      this.setState({
        showShareOptions: !this.state.showShareOptions
      });
    } else if (!!this.shareDropdown && this.shareDropdown.contains(e.target)) {
    } else {
      this.handleOnShareClickOutside();
    }
  };

  handleOnShareClickOutside() {
    this.setState({ showShareOptions: false });
  }

  renderShare() {
    const { showShareOptions } = this.state;
    const socials = [
      {
        icon: SocialFacebookPng,
        shareButton: FacebookShareButton
      },
      {
        icon: SocialLinkedinPng,
        shareButton: LinkedinShareButton
      },
      {
        icon: SocialTwitterPng,
        shareButton: TwitterShareButton
      }
    ];

    return (
      <ShareDiv ref={shareDiv => (this.shareDiv = shareDiv)}>
        <ShareIconDiv ref={shareIconDiv => (this.shareIconDiv = shareIconDiv)}>
          <Img src={SharePng} />
        </ShareIconDiv>
        {showShareOptions && (
          <ShareOptionsDropdown
            ref={shareDropdown => (this.shareDropdown = shareDropdown)}
          >
            {socials.map((social, index) => (
              <social.shareButton
                key={index}
                url={`${externalLinks.userGuideShareLink}${routes.home}`}
                {...social.additionalProps}
                style={{ height: "50px" }}
              >
                <SocialIconDiv>
                  <ShareIconImg src={social.icon} />
                </SocialIconDiv>
              </social.shareButton>
            ))}
          </ShareOptionsDropdown>
        )}
      </ShareDiv>
    );
  }

  handleOnDarkOverlayClick() {
    this.setState({
      openSection: {}
    });
  }
  render() {
    const { copy } = this.props;
    const { openSection } = this.state;
    const { whatSection, whySection, howSection } = copy;
    return (
      <Container>
        <NavItemsDiv>
          {this.renderNavMainSections(whatSection, theme.colorTurqoise)}
          {this.renderNavMainSections(whySection, theme.colorPurple)}
          {this.renderNavMainSections(howSection, theme.colorGreen)}
        </NavItemsDiv>
        {this.renderShare()}

        {this.renderWhatDropdown()}
        {this.renderWhyDropdown()}
        {this.renderHowDropdown()}
        <DarkOverlay
          isVisible={!_isEmpty(openSection)}
          onClick={() => this.handleOnDarkOverlayClick()}
        />
      </Container>
    );
  }
}

export default withRouter(MobileNavContainer);

const Container = styled.div`
  display: flex;
  position: fixed;
  width: 100%
  top: ${props => props.theme.headerBarHeight};
  @media (min-width: 800px) {
    display: none;
  }
`;

const NavItemsDiv = styled.div`
  display: flex;
  width: calc(100vw - 50px);
  @media (min-width: 600px) {
    width: calc(100vw-200px);
  }
  height: ${props => props.theme.headerBarHeight};
  background: white;
`;

const NavItemDiv = styled.div`
  cursor: pointer;
  flex: 1;
  padding: 0.5em;
  display: flex;
  align-items: center;
  justify-content: center;
  color: white;
  text-transform: uppercase;
  font-weight: bold;
`;

const NavItem = styled(NavItemDiv)`
  background: ${props => props.bgColor};
`;

const ShareDiv = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${props => props.theme.colorBlack};
  color: white;
  flex-direction: column;
  &:focus {
    outline: none;
    -webkit-appearance: none;
  }
`;

const ShareIconDiv = styled.div`
  cursor: pointer;
  width: 50px;
  height: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Img = styled.img`
  max-height: 100%;
  height: 60%;
`;

const Dropdown = styled.div`
  display: ${props => (props.isVisible ? "block" : "none")};
  position: absolute;
  width: 100%;
  top: ${props => props.theme.headerBarHeight};
  a {
    text-decoration: none !important;
  }
  z-index: 2;
`;

const SubSectionDiv = styled.div`
  background: ${props => props.bgColor};
  color: white;
  border-bottom: 1px solid white;
  font-size: 14px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  span {
    display: inline-block;
    font-size: 16px;
  }
`;

const SubSectionTitle = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0.5em 2em;

}`;

const TopicsDiv = styled.div`
  display: ${props => (props.isVisible ? "block" : "none")}
  background: ${props => props.bgColor};
  color: white;
  a {
    text-decoration: none !important;
    color: inherit !important;
  }
`;

const TopicTitle = styled.div`
  border-bottom: 1px solid white;
  padding: 0.5em 2em 0.5em 4em;
  display: flex;
  justify-content: space-between;
`;

const ShareOptionsDropdown = styled.div`
  background: ${props => props.theme.colorBlack};
  height: 140px;
  width: 100%;
  padding-top: 1em;
  div {
    &:focus {
      outline: none;
      -webkit-appearance: none;
    }
  }
`;

const SocialIconDiv = styled.div`
  height: 20px;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ShareIconImg = styled.img`
  height: 100%;
  width: auto;
`;

const DarkOverlay = styled.div`
  position: fixed;
  background: black;
  opacity: 0.3;
  height: 100vh;
  width: 100vw;
  z-index: 1;
  display: ${props => (props.isVisible ? "block" : "none")};
  top: ${props => `${props.theme.verticalOffsetMobile}px`};
`;
