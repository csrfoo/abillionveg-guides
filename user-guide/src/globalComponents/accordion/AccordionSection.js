import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import styled from "styled-components";

class AccordionSection extends Component {
  static propTypes = {
    children: PropTypes.instanceOf(Object).isRequired,
    isOpen: PropTypes.bool.isRequired,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
  };

  handleOnClick(id) {
    this.props.onClick(id);
  }

  render() {
    const {
      isOpen,
      label,
      isFirstSection,
      isMainAccordion,
      sectionColor,
      isInnerAccordionActive,
      id
    } = this.props;

    return (
      <Container>
        <LabelDiv
          onClick={() => this.handleOnClick(id)}
          isOpen={isOpen}
          isMainAccordion={isMainAccordion}
          sectionColor={sectionColor}
          showActiveColor={!isMainAccordion && isInnerAccordionActive}
        >
          <LabelText
            isFirstSection={isFirstSection}
            isMainAccordion={isMainAccordion}
          >
            {!isOpen && (
              <div>
                <SectionIndicator sectionColor={sectionColor}>
                  +
                </SectionIndicator>
                <p>{label}</p>
              </div>
            )}
            {isOpen && (
              <div>
                <SectionIndicator sectionColor={sectionColor}>
                  -
                </SectionIndicator>
                <p>{label}</p>
              </div>
            )}
          </LabelText>
        </LabelDiv>
        {isOpen && (
          <BulletPointsDiv isMainAccordion={isMainAccordion}>
            {this.props.children}
          </BulletPointsDiv>
        )}
      </Container>
    );
  }
}

export default withRouter(AccordionSection);

const Container = styled.div``;

const LabelDiv = styled.div`
  font-weight: ${props => (props.isOpen ? 600 : "initial")};
  cursor: pointer;
  display: flex;
  justify-content: space-between;
  align-items: center;
  text-transform: ${props => props.isMainAccordion && "uppercase"};
  color: ${props =>
    props.isMainAccordion
      ? props.theme.colorBlack
      : props.showActiveColor
        ? props.sectionColor
        : props.theme.colorLightGrey};
  &:hover {
    color: ${props => props.sectionColor} !important;
  }
`;

const LabelText = styled.div`
  font-size: ${props => (props.isMainAccordion ? "20px" : "14px")}
  margin-block-start: ${props => (props.isFirstSection ? 0 : "0.83em")};

  p {
    display: inline-block;
    margin: 0;
    font-size: 16px;
  }
`;

const SectionIndicator = styled.span`
  display: inline-block;
  width: 15px;
  color: ${props => props.sectionColor};
`;

const BulletPointsDiv = styled.div`
  @media (min-width: 600px) {
    padding: 5px 15px;
  }
  a {
    font-size: 16px;
    text-decoration: none;
    margin-top: 5px;

    p {
      margin-top: 5px;
      margin-bottom: 5px;
    }
  }
`;
