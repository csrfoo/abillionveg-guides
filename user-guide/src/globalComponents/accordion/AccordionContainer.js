import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import AccordionSection from "./AccordionSection";

class AccordionContainer extends Component {
  static propTypes = {
    children: PropTypes.instanceOf(Object).isRequired
  };

  render() {
    const {
      children,
      isOpen,
      isMainAccordion,
      sectionColor,
      isInnerAccordionActive
    } = this.props;

    return (
      <Container>
        {React.Children.map(children, (child, index) => (
          <AccordionSection
            isOpen={isOpen}
            label={child.props.label}
            id={child.props.id}
            onClick={id => this.props.onClick(id)}
            isFirstSection={index === 0}
            key={index}
            isMainAccordion={isMainAccordion}
            sectionColor={sectionColor}
            isInnerAccordionActive={isInnerAccordionActive}
          >
            {child.props.children}
          </AccordionSection>
        ))}
      </Container>
    );
  }
}

export default AccordionContainer;

const Container = styled.div`
  width: 100%;
`;
