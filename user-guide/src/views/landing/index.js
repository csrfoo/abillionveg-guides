import React, { Component } from "react";
import PropTypes from "prop-types";

import LandingContainer from "./LandingContainer.js";

class Landing extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  render() {
    const { copy } = this.props;
    return <LandingContainer copy={copy} />;
  }
}

export default Landing;
