import React, { Component } from "react";
import styled, { keyframes } from "styled-components";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";

import theme from "../../globalStyles.js";

import LandingPageBgMobilePng from "../../assets/landing-page-bg-mobile.png";
import LandingPageBgPng from "../../assets/landing-page-bg.png";
import abvLogoWhitePng from "../../assets/abv-logo-white.png";
import DownChevronPng from "../../assets/down-chevron.png";

import { scrollBy } from "../../utils/helper.js";

const pulse = keyframes`
  0%, 20%, 50%, 80%, 100% {
    transform: translateY(0);
  }
  40% {
  transform: translateY(-30px);
  }
  60% {
    transform: translateY(-15px);
  }
  `;

class LandingContainer extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      bgContainerIsVisible: true,
      landingScreenIsVisible:
        window.innerWidth < theme.mobileDesktopDesignBreakpoint ? false : true
    };
    this.hideLandingPage = this.hideLandingPage.bind(this);
  }

  componentDidMount() {
    if (window.innerWidth >= theme.mobileDesktopDesignBreakpoint) {
      if (this.props.location.pathname !== "/") {
        this.setState({
          landingScreenIsVisible: false,
          bgContainerIsVisible: false
        });
      }
    } else {
      if (this.props.location.pathname !== "/") {
        this.setState({
          landingScreenIsVisible: false,
          bgContainerIsVisible: false
        });
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.location.pathname !== this.props.location.pathname) {
      if (window.innerWidth < theme.mobileDesktopDesignBreakpoint) {
        if (this.props.location.pathname !== "/") {
          this.setState({
            bgContainerIsVisible: false
          });
        } else {
          this.setState({
            bgContainerIsVisible: true
          });
        }
      } else if (window.innerWidth >= theme.mobileDesktopDesignBreakpoint) {
        scrollBy(window.innerHeight, this.hideLandingPage);
      }
    }
  }

  hideLandingPage() {
    this.setState({
      landingScreenIsVisible: false,
      bgContainerIsVisible: false
    });
    window.scrollTo(0, 0);
  }

  handleOnChevronClick() {
    if (this.props.location.pathname === "/") {
      this.props.history.push(
        `/${this.props.copy.whatSection.id}/${
          this.props.copy.whatSection.subSections[0].id
        }`
      );
    }
  }

  handleOnLogoClick() {
    if (window.innerWidth < theme.mobileDesktopDesignBreakpoint) {
      this.props.history.push("/");
    }
  }

  render() {
    const { copy } = this.props;
    const { landing } = copy;
    const { bgContainerIsVisible, landingScreenIsVisible } = this.state;

    return (
      <BgContainer
        bgContainerIsVisible={bgContainerIsVisible}
        landingScreenIsVisible={landingScreenIsVisible}
      >
        <Container>
          <HeaderBar>
            <LogoDiv onClick={() => this.handleOnLogoClick()}>
              <Img src={abvLogoWhitePng} />
            </LogoDiv>
          </HeaderBar>

          <Body
            bgContainerIsVisible={bgContainerIsVisible}
            landingScreenIsVisible={landingScreenIsVisible}
          >
            <GoingVeganH1>{landing.goingVegan}</GoingVeganH1>
            <DoingItTheRightWayH2>
              {landing.doingItTheRightWay}
            </DoingItTheRightWayH2>
            <KickstartH3>{landing.kickstartYourJourney}</KickstartH3>
            <LetsGoH4>{landing.letsGo}</LetsGoH4>
            <ChevronDiv onClick={() => this.handleOnChevronClick()}>
              <Img src={DownChevronPng} />
            </ChevronDiv>
          </Body>
        </Container>
      </BgContainer>
    );
  }
}

export default withRouter(LandingContainer);

const BgContainer = styled.div`
  height: ${props => (props.bgContainerIsVisible ? "100vh" : 0)}
  background-image: url(${LandingPageBgMobilePng});
  background-size: cover;
  background-repeat: no-repeat;
  @media (min-width: 800px) {
    background-image: url(${LandingPageBgPng});
    height: ${props => (props.landingScreenIsVisible ? "100vh" : "50px")}
  }

`;

const Container = styled.div`
  // max-width: ${props => props.theme.maxContentWidth};
  margin: 0 auto;
  height: 100%;
`;

const LogoDiv = styled.div`
  width: 100px;
`;

const Img = styled.img`
  width: 100%;
`;

const HeaderBar = styled.div`
  height: ${props => props.theme.headerBarHeight}
  background: ${props => props.theme.colorOrange};
  display: flex;
  align-items: center;
  padding: 0em 1em;
  position: fixed;
  width: 100%;
  top: 0
`;

const Body = styled.div`
  display: ${props =>
    props.bgContainerIsVisible || props.landingScreenIsVisible
      ? "flex"
      : "none"};
  justify-content: center;
  height: 100vh;
  flex-direction: column;
`;

const GoingVeganH1 = styled.h1`
  font-size: 24px;
  background: white;
  text-align: center;
  white-space: nowrap;
  text-transform: uppercase;
  color: ${props => props.theme.colorOrange};
  margin: 0 auto;
  padding: 0.5em 1em;
  font-weight: 800;
  @media (min-width: 900px) {
    font-size: 40px;
    letter-spacing: 6px;
    margin-top: ${props => props.theme.headerBarHeight};
  }
`;

const DoingItTheRightWayH2 = styled.h2`
  color: black;
  font-size: 24px;
  text-transform: uppercase;
  margin: 1em auto;
  font-weight: 800;
  margin: 0.5em auto;
  width: 50%;
  text-align: center;
  @media (min-width: 900px) {
    font-size: 40px;
    letter-spacing: 6px;
  }
`;
const KickstartH3 = styled.h3`
  color: black;
  margin: 0 auto;
  text-align: center;
  font-weight: 400 !important;
  font-size: 20px;
  @media (min-width: 900px) {
    font-size: 30px;
    width: 40%;
    line-height: 40px;
  }
`;

const LetsGoH4 = styled.h4`
  display: none;
  @media (min-width: 900px) {
    display: block;
    text-align: center;
    font-size: 28px;
    margin-bottom: 0;
  }
`;

const ChevronDiv = styled.div`
  display: none;
  @media (min-width: 800px) {
    width: 100px;
    margin: 0 auto;
    margin-top: ${props => props.theme.headerBarHeight};

    cursor: pointer;
    display: block;
    img {
      animation: ${pulse} 2s 0s ease-out infinite;
    }
  }
`;
