import React, { Component } from "react";
import styled from "styled-components";
import { withRouter, Link } from "react-router-dom";
import _find from "lodash/find";
import _findIndex from "lodash/findIndex";

import PropTypes from "prop-types";

import Button from "../../../../globalComponents/button.js";

import theme from "../../../../globalStyles.js";
import { createMarkup } from "../../../../utils/helper.js";

import ArrowPng from "../../../../assets/arrow.png";
import ArrowBackPng from "../../../../assets/arrow-back.png";

class WhatContainer extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      currSection: props.match.params.subSectionId
    };
    this.container = React.createRef();
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.match.params.subSectionId !==
      this.props.match.params.subSectionId
    ) {
      this.setState({
        currSection: this.props.match.params.subSectionId
      });
      this.container.current.scrollTo(0, 0);
    }
  }

  getNextPageLink(currIndex) {
    const { whatSection, whySection } = this.props.copy;
    let result;
    let title;
    let url;
    if (currIndex < whatSection.subSections.length - 1) {
      title = whatSection.subSections[currIndex + 1].title;
      url = `/${whatSection.id}/${whatSection.subSections[currIndex + 1].id}`;
    } else if (currIndex === whatSection.subSections.length - 1) {
      title = !!whySection.subSections[0].topics
        ? whySection.subSections[0].topics[0].title
        : whySection.subSections[0].title;

      url = !!whySection.subSections[0].topics
        ? `/${whySection.id}/${whySection.subSections[0].id}/${
            whySection.subSections[0].topics[0].id
          }`
        : `/${whySection.id}/${whySection.subSections[0].id}`;
    }
    result = (
      <Link to={url}>
        <Button
          bgColor={theme.colorTurqoise}
          text={title}
          icon={ArrowPng}
          iconPosition="right"
        />
      </Link>
    );

    return result;
  }

  getPreviousPageLink(currIndex) {
    const { whatSection } = this.props.copy;
    let result;
    let title;
    let url;
    if (currIndex === 0) {
      result = "";
    } else if (currIndex < whatSection.subSections.length) {
      title =
        !!whatSection.subSections[currIndex - 1] &&
        whatSection.subSections[currIndex - 1].title;
      url = `/${whatSection.id}/${!!whatSection.subSections[currIndex - 1] &&
        whatSection.subSections[currIndex - 1].id}`;
      result = (
        <Link to={url}>
          <Button
            bgColor={theme.colorLightGrey}
            text={title}
            icon={ArrowBackPng}
            iconPosition="left"
          />
        </Link>
      );
    }

    return result;
  }

  render() {
    const { copy } = this.props;
    const { whatSection } = copy;
    const { currSection } = this.state;

    const currContent = _find(whatSection.subSections, { id: currSection });
    const currContentIndex = _findIndex(whatSection.subSections, {
      id: currSection
    });

    return (
      <Container ref={this.container}>
        <H2>{!!currContent && currContent.title}</H2>
        <P
          dangerouslySetInnerHTML={createMarkup(
            !!currContent && currContent.body
          )}
        />
        <PageLinksDiv>
          <PageLink>{this.getPreviousPageLink(currContentIndex)}</PageLink>
          <PageLink>{this.getNextPageLink(currContentIndex)}</PageLink>
        </PageLinksDiv>
      </Container>
    );
  }
}

export default withRouter(WhatContainer);

const Container = styled.div`
  color: ${props => props.theme.colorBlack}
  flex: 4;
  overflow-y: scroll;
  @media (min-width: 800px) {
    padding-right: 2em;
  }


`;

const H2 = styled.h2`
  font-size: 28px;
  margin-top: 0;
`;

const P = styled.p`
  color: ${props => props.theme.colorGreyP};
  font-size: 18px;
`;

const PageLinksDiv = styled.div`
  justify-content: space-around;
  display: flex;
  flex-wrap: wrap-reverse;
  @media (min-width: 600px) {
    justify-content: space-between;
  }
`;

const PageLink = styled.p`
  color: ${props => props.theme.colorTurqoise};
  font-weight: 500;
  text-align: right;
  a {
    text-decoration: none !important;
    color: inherit !important;
  }
`;
