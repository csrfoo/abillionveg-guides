import React, { Component } from "react";
import PropTypes from "prop-types";

import WhatContainer from "./WhatContainer.js";

class WhatSection extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  render() {
    const { copy } = this.props;

    return <WhatContainer copy={copy} />;
  }
}

export default WhatSection;
