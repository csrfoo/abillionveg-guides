import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

class Quote extends Component {
  static propTypes = {
    text: PropTypes.string,
    color: PropTypes.string
  };

  render() {
    const { text, color } = this.props;
    return (
      <Container>
        <P color={color}>{text}</P>
      </Container>
    );
  }
}

export default Quote;

const Container = styled.div`
  padding: 1em;
`;

const P = styled.p`
  font-weight: 600
  text-align: center;
  color: ${props => props.color};
`;
