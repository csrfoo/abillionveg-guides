import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

class FurtherReading extends Component {
  static propTypes = {
    links: PropTypes.array.isRequired
  };

  render() {
    const { bgColor, linkColor, title, links } = this.props;
    return (
      <Container bgColor={bgColor} linkColor={linkColor}>
        <P>{title}</P>
        {links.map((link, index) => (
          <a
            href={link.url}
            key={index}
            target="_blank"
            rel="noopener noreferrer"
          >
            {link.title}
          </a>
        ))}
      </Container>
    );
  }
}

export default FurtherReading;

const Container = styled.div`
  background: ${props => props.bgColor};
  border-radius: 5px;
  padding: 2em;
  a {
    text-decoration: underline;
    color: ${props => props.linkColor};
    display: block;
  }
`;

const P = styled.div`
  font-weight: 700;
`;
