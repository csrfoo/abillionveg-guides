import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

class ImageWithCaption extends Component {
  static propTypes = {
    imagePosition: PropTypes.string,
    color: PropTypes.string
  };

  render() {
    const { image, imagePosition, color, caption } = this.props;
    return (
      <Container>
        <ImageDiv>
          <Image src={image} isLeft={imagePosition === "left"} />
        </ImageDiv>
        <Caption color={color} isLeft={imagePosition === "left"}>
          {caption}
        </Caption>
      </Container>
    );
  }
}

export default ImageWithCaption;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  @media (min-width: 600px) {
    flex-direction: row;
  }
  margin-bottom: 1em;
`;

const ImageDiv = styled.div`
  width: 100%
  text-align: center;
  @media (min-width: 600px) {
    width: 25%
    order: ${props => (props.isLeft ? 1 : 2)};
  }
`;

const Image = styled.img`
  width: 75%;
  margin: 0 auto;
  @media (min-width: 600px) {
    width: 100%
    max-width: 100%;
  }
`;

const Caption = styled.p`
  width: 75%
  font-weight: 700;
  color: ${props => props.color};
  padding: 0 2em;
  @media (min-width: 600px) {
    order: ${props => (props.isLeft ? 2 : 1)};
  }
`;
