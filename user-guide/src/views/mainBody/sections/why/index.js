import React, { Component } from "react";
import PropTypes from "prop-types";

import WhyContainer from "./WhyContainer.js";

class WhySection extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  render() {
    const { copy } = this.props;

    return <WhyContainer copy={copy} />;
  }
}

export default WhySection;
