import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { withRouter, Link } from "react-router-dom";
import _find from "lodash/find";
import _findIndex from "lodash/findIndex";

import Button from "../../../../globalComponents/button.js";
import FurtherReading from "../local_components/furtherReading.js";
import ImageWithCaption from "../local_components/imageWithCaption.js";
import Quote from "../local_components/quote.js";

import theme from "../../../../globalStyles.js";
import { createMarkup } from "../../../../utils/helper.js";

import ArrowPng from "../../../../assets/arrow.png";
import ArrowBackPng from "../../../../assets/arrow-back.png";

class WhyContainer extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      currSubSection: _find(props.copy.whySection.subSections, {
        id: props.match.params.subSectionId
      })
    };
    this.container = React.createRef();
  }

  componentDidMount() {
    this.setState({
      currSubSection: _find(this.props.copy.whySection.subSections, {
        id: this.props.match.params.subSectionId
      })
    });
  }

  componentDidUpdate(prevProps) {
    !!this.container &&
      !!this.container.current &&
      this.container.current.scrollTo(0, 0);

    if (
      prevProps.match.params.subSectionId !==
      this.props.match.params.subSectionId
    ) {
      this.setState({
        currSubSection: _find(this.props.copy.whySection.subSections, {
          id: this.props.match.params.subSectionId
        })
      });
    }
  }

  getNextPageLink(subSectionIndex, topicIndex) {
    let result;
    let title;
    let url;
    const { whySection, howSection } = this.props.copy;
    const indexOfLastSubSection = whySection.subSections.length - 1;
    const lastTopicArray =
      whySection.subSections[whySection.subSections.length - 1].topics;
    const indexOfLastTopic = lastTopicArray.length - 1;

    if (
      subSectionIndex === indexOfLastSubSection &&
      topicIndex === indexOfLastTopic
    ) {
      title = !!howSection.subSections[0].topics
        ? howSection.subSections[0].topics[0].title
        : howSection.subSections[0].title;
      url = !!howSection.subSections[0].topics
        ? `/${howSection.id}/${howSection.subSections[0].id}/${
            howSection.subSections[0].topics[0].id
          }`
        : `/${howSection.id}/${howSection.subSections[0].id}`;
    } else {
      if (
        !!whySection.subSections[subSectionIndex] &&
        whySection.subSections[subSectionIndex].topics[topicIndex + 1] ===
          undefined
      ) {
        title = whySection.subSections[subSectionIndex + 1].topics[0].title;
        url = `/${whySection.id}/${
          whySection.subSections[subSectionIndex + 1].id
        }/${whySection.subSections[subSectionIndex + 1].topics[0].id}`;
      } else {
        title =
          !!whySection.subSections[subSectionIndex] &&
          whySection.subSections[subSectionIndex].topics[topicIndex + 1].title;

        url = `/${!!whySection && whySection.id}/${!!whySection.subSections[
          subSectionIndex
        ] && whySection.subSections[subSectionIndex].id}/${!!whySection
          .subSections[subSectionIndex] &&
          whySection.subSections[subSectionIndex].topics[topicIndex + 1].id}`;
      }
    }
    result = (
      <Link to={url}>
        <Button
          bgColor={theme.colorPurple}
          text={title}
          icon={ArrowPng}
          iconPosition="right"
        />
      </Link>
    );

    return result;
  }

  getPreviousPageLink(subSectionIndex, topicIndex) {
    const { whatSection, whySection } = this.props.copy;
    let result;
    let title;
    let url;
    if (subSectionIndex === 0 && topicIndex === 0) {
      title = whatSection.subSections[whatSection.subSections.length - 1].title;
      url = `/${whatSection.id}/${
        whatSection.subSections[whatSection.subSections.length - 1].id
      }`;
    } else {
      if (
        !!whySection.subSections[subSectionIndex] &&
        whySection.subSections[subSectionIndex].topics[topicIndex - 1] ===
          undefined
      ) {
        title =
          !!whySection.subSections[subSectionIndex - 1] &&
          whySection.subSections[subSectionIndex - 1].topics[
            whySection.subSections[subSectionIndex - 1].topics.length - 1
          ].title;
        url = `/${!!whySection && whySection.id}/${!!whySection.subSections[
          subSectionIndex - 1
        ] && whySection.subSections[subSectionIndex - 1].id}/${!!whySection
          .subSections[subSectionIndex - 1] &&
          whySection.subSections[subSectionIndex - 1].topics[
            whySection.subSections[subSectionIndex - 1].topics.length - 1
          ].id}`;
      } else {
        title =
          !!whySection.subSections[subSectionIndex] &&
          whySection.subSections[subSectionIndex].topics[topicIndex - 1].title;

        url = `/${!!whySection && whySection.id}/${!!whySection.subSections[
          subSectionIndex
        ] && whySection.subSections[subSectionIndex].id}/${!!whySection
          .subSections[subSectionIndex] &&
          whySection.subSections[subSectionIndex].topics[topicIndex - 1].id}`;
      }
    }
    result = (
      <Link to={url}>
        <Button
          bgColor={theme.colorLightGrey}
          text={title}
          icon={ArrowBackPng}
          iconPosition="left"
        />
      </Link>
    );

    return result;
  }

  render() {
    const { currSubSection } = this.state;
    const currTopic = this.props.match.params.topicId;

    const currContent = _find(!!currSubSection && currSubSection.topics, {
      id: currTopic
    });

    const currSubSectionIndex = _findIndex(
      this.props.copy.whySection.subSections,
      {
        id: !!currSubSection && currSubSection.id
      }
    );

    const currTopicIndex = _findIndex(
      !!currSubSection && currSubSection.topics,
      {
        id: currTopic
      }
    );

    return (
      <Container ref={this.container}>
        {!!currContent &&
          currContent.whatsWrong && (
            <React.Fragment>
              <H2>{!!currContent && currContent.whatsWrong.title}</H2>
              <P
                dangerouslySetInnerHTML={createMarkup(
                  !!currContent && currContent.whatsWrong.blurb
                )}
              />
            </React.Fragment>
          )}
        {!!currContent &&
          currContent.whatsWrongQuotes &&
          currContent.whatsWrongQuotes.map((quote, index) => (
            <Quote color={theme.colorPurple} text={quote} key={index} />
          ))}
        {!!currContent &&
          currContent.whatsWrongImages &&
          currContent.whatsWrongImages.map((image, index) => (
            <ImageWithCaption
              key={index}
              image={image.imageSrc}
              caption={image.caption}
              color={theme.colorPurple}
              imagePosition={image.imagePosition}
            />
          ))}
        {!!currContent &&
          currContent.whatsTheStory && (
            <React.Fragment>
              <H2>{!!currContent && currContent.whatsTheStory.title}</H2>
              <P
                dangerouslySetInnerHTML={createMarkup(
                  !!currContent && currContent.whatsTheStory.blurb
                )}
              />
            </React.Fragment>
          )}
        {!!currContent &&
          currContent.whatsTheStoryImages &&
          currContent.whatsTheStoryImages.map((image, index) => (
            <ImageWithCaption
              key={index}
              image={image.imageSrc}
              caption={image.caption}
              color={theme.colorPurple}
              imagePosition={image.imagePosition}
            />
          ))}
        {!!currContent &&
          currContent.paras && (
            <React.Fragment>
              <H2>{!!currContent && currContent.paras.title}</H2>
              <P
                dangerouslySetInnerHTML={createMarkup(
                  currContent.paras.content
                )}
              />
            </React.Fragment>
          )}
        {!!currContent &&
          currContent.blurb && (
            <P dangerouslySetInnerHTML={createMarkup(currContent.blurb)} />
          )}
        {!!currContent &&
          currContent.paraQuotes &&
          currContent.paraQuotes.map((quote, index) => (
            <Quote color={theme.colorPurple} text={quote} key={index} />
          ))}
        {!!currContent &&
          currContent.furtherReading && (
            <FurtherReading
              links={currContent.furtherReading.links}
              bgColor={theme.colorLightestPurple}
              linkColor={theme.colorPurple}
              title={currContent.furtherReading.title}
            />
          )}
        <PageLinksDiv>
          <PageLink>
            {this.getPreviousPageLink(currSubSectionIndex, currTopicIndex)}
          </PageLink>
          <PageLink>
            {this.getNextPageLink(currSubSectionIndex, currTopicIndex)}
          </PageLink>
        </PageLinksDiv>
      </Container>
    );
  }
}

export default withRouter(WhyContainer);

const Container = styled.div`
  flex: 4;
  overflow-y: scroll;
  a {
    color: inherit !important;
  }
  li {
    margin-bottom: 1em;
  }
  @media (min-width: 800px) {
    padding-right: 2em;
  }
`;

const H2 = styled.h2`
  font-size: 28px;
  margin-top: 0;
`;

const P = styled.p`
  color: ${props => props.theme.colorGreyP};
  font-size: 18px;
`;

const PageLinksDiv = styled.div`
  justify-content: space-around;
  display: flex;
  flex-wrap: wrap-reverse;
  @media (min-width: 600px) {
    justify-content: space-between;
  }
`;

const PageLink = styled.p`
  color: ${props => props.theme.colorTurqoise};
  font-weight: 500;
  text-align: right;
  a {
    text-decoration: none !important;
    color: inherit !important;
  }
`;
