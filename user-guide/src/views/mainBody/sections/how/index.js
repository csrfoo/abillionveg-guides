import React, { Component } from "react";
import PropTypes from "prop-types";

import HowContainer from "./HowContainer.js";

class HowSection extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  render() {
    const { copy } = this.props;

    return <HowContainer copy={copy} />;
  }
}

export default HowSection;
