import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { withRouter, Link } from "react-router-dom";
import _find from "lodash/find";
import _findIndex from "lodash/findIndex";

import { createMarkup } from "../../../../utils/helper.js";
import theme from "../../../../globalStyles.js";

import Button from "../../../../globalComponents/button.js";
import Quote from "../local_components/quote.js";

import ArrowPng from "../../../../assets/arrow.png";
import ArrowBackPng from "../../../../assets/arrow-back.png";

class HowContainer extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    const { howSection } = this.props.copy;
    const lastTopicArray =
      howSection.subSections[howSection.subSections.length - 1].topics;

    this.state = {
      indexOfLastSubSection: howSection.subSections.length - 1,
      indexOfLastTopic: !!lastTopicArray ? lastTopicArray.length - 1 : undefined
    };
    this.container = React.createRef();
  }

  componentDidUpdate() {
    !!this.container &&
      !!this.container.current &&
      this.container.current.scrollTo(0, 0);
  }

  getNextPageLink(subSectionIndex, topicIndex) {
    let result;
    let title;
    let url;
    const { howSection } = this.props.copy;
    const { indexOfLastTopic, indexOfLastSubSection } = this.state;

    if (
      subSectionIndex === indexOfLastSubSection &&
      topicIndex === indexOfLastTopic
    ) {
      result = "";
    } else {
      if (
        howSection.subSections[subSectionIndex + 1].topics === undefined &&
        !!howSection.subSections[subSectionIndex] &&
        howSection.subSections[subSectionIndex].topics === undefined
      ) {
        title = howSection.subSections[subSectionIndex + 1].title;
        url = `/${howSection.id}/${
          howSection.subSections[subSectionIndex + 1].id
        }`;
      } else {
        if (
          !!howSection.subSections[subSectionIndex] &&
          howSection.subSections[subSectionIndex].topics[topicIndex + 1] ===
            undefined
        ) {
          title = !!howSection.subSections[subSectionIndex + 1].topics
            ? howSection.subSections[subSectionIndex + 1].topics[0].title
            : howSection.subSections[subSectionIndex + 1].title;

          url = `/${howSection.id}/${
            howSection.subSections[subSectionIndex + 1].id
          }/${
            !!howSection.subSections[subSectionIndex + 1].topics
              ? howSection.subSections[subSectionIndex + 1].topics[0].id
              : ""
          }`;
        } else {
          title =
            !!howSection.subSections[subSectionIndex] &&
            howSection.subSections[subSectionIndex].topics[topicIndex + 1]
              .title;
          url = `/${!!howSection && howSection.id}/${!!howSection.subSections[
            subSectionIndex
          ] && howSection.subSections[subSectionIndex].id}/${!!howSection
            .subSections[subSectionIndex] &&
            howSection.subSections[subSectionIndex].topics[topicIndex + 1].id}`;
        }
      }
      result = (
        <Link to={url}>
          <Button
            bgColor={theme.colorGreen}
            text={title}
            icon={ArrowPng}
            iconPosition="right"
          />
        </Link>
      );
    }
    return result;
  }

  getPreviousPageLink(subSectionIndex, topicIndex) {
    let result;
    let title;
    let url;
    const { howSection, whySection } = this.props.copy;
    const { indexOfLastTopic, indexOfLastSubSection } = this.state;

    if (
      subSectionIndex === indexOfLastSubSection &&
      topicIndex === indexOfLastTopic
    ) {
      // LAST TOPIC OF LAST SECTION
      title = howSection.subSections[indexOfLastSubSection - 1].title;
      url = `/${howSection.id}/${
        howSection.subSections[indexOfLastSubSection - 1].id
      }`;
    } else if (subSectionIndex === 0 && topicIndex === 0) {
      // FIRST TOPIC OF FIRST SECTION
      title =
        !!whySection.subSections[whySection.subSections.length - 1] &&
        whySection.subSections[whySection.subSections.length - 1].topics[
          whySection.subSections[whySection.subSections.length - 1].topics
            .length - 1
        ].title;
      url = `/${whySection.id}/${
        whySection.subSections[whySection.subSections.length - 1].id
      }/${
        whySection.subSections[whySection.subSections.length - 1].topics[
          whySection.subSections[whySection.subSections.length - 1].topics
            .length - 1
        ].id
      }`;
    } else {
      if (
        !!howSection.subSections[subSectionIndex] &&
        !!howSection.subSections[subSectionIndex].topics &&
        howSection.subSections[subSectionIndex].topics[topicIndex - 1] ===
          undefined
      ) {
        title =
          howSection.subSections[subSectionIndex - 1].topics[
            howSection.subSections[subSectionIndex - 1].topics.length - 1
          ].title;
        url = `/${howSection.id}/${
          howSection.subSections[subSectionIndex - 1].id
        }/${
          howSection.subSections[subSectionIndex - 1].topics[
            howSection.subSections[subSectionIndex - 1].topics.length - 1
          ].id
        }`;
      } else if (
        !!howSection.subSections[subSectionIndex] &&
        howSection.subSections[subSectionIndex].topics === undefined &&
        howSection.subSections[subSectionIndex - 1].topics !== undefined
      ) {
        title =
          howSection.subSections[subSectionIndex - 1].topics[
            howSection.subSections[subSectionIndex - 1].topics.length - 1
          ].title;
        url = `/${howSection.id}/${
          howSection.subSections[subSectionIndex - 1].id
        }/${
          howSection.subSections[subSectionIndex - 1].topics[
            howSection.subSections[subSectionIndex - 1].topics.length - 1
          ].id
        }`;
      } else {
        title =
          !!howSection.subSections[subSectionIndex] &&
          howSection.subSections[subSectionIndex].topics[topicIndex - 1].title;

        url = `/${!!howSection && howSection.id}/${!!howSection.subSections[
          subSectionIndex
        ] && howSection.subSections[subSectionIndex].id}/${!!howSection
          .subSections[subSectionIndex] &&
          howSection.subSections[subSectionIndex].topics[topicIndex - 1].id}`;
      }
    }

    result = (
      <Link to={url}>
        <Button
          bgColor={theme.colorLightGrey}
          text={title}
          icon={ArrowBackPng}
          iconPosition="left"
        />
      </Link>
    );

    return result;
  }

  render() {
    const { copy } = this.props;
    const { howSection } = copy;
    const { params } = this.props.match;
    const { indexOfLastTopic, indexOfLastSubSection } = this.state;

    let currContent;

    if (params.topicId === undefined) {
      currContent = _find(howSection.subSections, {
        id: params.subSectionId
      });
    } else {
      currContent = _find(
        !!_find(howSection.subSections, {
          id: params.subSectionId
        }) &&
          _find(howSection.subSections, {
            id: params.subSectionId
          }).topics,
        { id: params.topicId }
      );
    }

    const currSubSectionIndex = _findIndex(
      this.props.copy.howSection.subSections,
      {
        id: params.subSectionId
      }
    );

    const currTopicIndex =
      params.topicId !== undefined
        ? _findIndex(
            !!this.props.copy.howSection.subSections[currSubSectionIndex] &&
              this.props.copy.howSection.subSections[currSubSectionIndex]
                .topics,
            {
              id: params.topicId
            }
          )
        : undefined;

    const showDownloadGuide =
      indexOfLastTopic === currTopicIndex &&
      indexOfLastSubSection === currSubSectionIndex;

    return (
      <Container ref={this.container}>
        {!!currContent &&
          currContent.paras && (
            <React.Fragment>
              <H2>{!!currContent && currContent.paras.title}</H2>
              <P
                dangerouslySetInnerHTML={createMarkup(
                  currContent.paras.content
                )}
              />
            </React.Fragment>
          )}
        {!!currContent &&
          currContent.paraQuotes &&
          currContent.paraQuotes.map((quote, index) => (
            <Quote color={theme.colorGreen} text={quote} key={index} />
          ))}

        <PageLinksDiv>
          <PageLink>
            {this.getPreviousPageLink(currSubSectionIndex, currTopicIndex)}
          </PageLink>
          <PageLink>
            {this.getNextPageLink(currSubSectionIndex, currTopicIndex)}
          </PageLink>
        </PageLinksDiv>

        {!!showDownloadGuide && (
          <DownloadGuideP
            dangerouslySetInnerHTML={createMarkup(copy.enjoyedTheGuide)}
          />
        )}
      </Container>
    );
  }
}

export default withRouter(HowContainer);

const Container = styled.div`
  overflow-y: scroll;
  flex: 4;
  li {
    margin-bottom: 1em;
  }
  a {
    color: inherit !important;
  }
  @media (min-width: 800px) {
    padding-right: 2em;
  }
`;

const H2 = styled.h2`
  font-size: 28px;
  margin-top: 0;
`;

const P = styled.p`
  color: ${props => props.theme.colorGreyP};
  font-size: 18px;
`;

const PageLinksDiv = styled.div`
  justify-content: space-around;
  display: flex;
  flex-wrap: wrap-reverse;
  @media (min-width: 600px) {
    justify-content: space-between;
  }
`;

const PageLink = styled.p`
  color: ${props => props.theme.colorTurqoise};
  font-weight: 500;
  text-align: right;
  a {
    text-decoration: none !important;
    color: inherit !important;
  }
`;

const DownloadGuideP = styled.p`
  background: ${props => props.theme.colorLightestPurple}
  font-weight: 600;
  text-align: center;
  color: ${props => props.theme.colorGrey}
  padding: 2em
  border-radius: 5px
`;
