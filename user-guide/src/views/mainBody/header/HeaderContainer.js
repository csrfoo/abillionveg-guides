import React, { Component } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";

import { getSectionColor } from "../../../utils/helper.js";

import Breadcrumbs from "../../../globalComponents/breadcrumbs";

class HeaderContainer extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  getSecondCrumbUrl(sectionObject, subSection) {
    let result;
    let sectionUrl = sectionObject.id;
    let subSectionUrl = subSection.id;
    let topicUrl = !!subSection.topics && subSection.topics[0].id;
    result = `/${sectionUrl}/${subSectionUrl}/${topicUrl}`;
    return result;
  }

  getSubSectionTitle(sectionObject) {
    const { params } = this.props.match;
    let subSectionId = params.subSectionId;
    let result = {};
    !!sectionObject &&
      sectionObject.subSections.forEach(subSection => {
        if (subSection.id === subSectionId) {
          result = {
            title: subSection.title,
            url: this.getSecondCrumbUrl(sectionObject, subSection)
          };
        }
      });
    return result;
  }

  getTopicTitle(sectionObject) {
    const { params } = this.props.match;
    let subSectionId = params.subSectionId;
    let topicId = params.topicId;
    let result = {};
    !!sectionObject &&
      sectionObject.subSections.forEach(subSection => {
        if (subSection.id === subSectionId) {
          !!subSection.topics &&
            subSection.topics.map(topic => {
              if (topic.id === topicId) {
                result = {
                  title: topic.title,
                  id: topic.id
                };
              }
              return result;
            });
          return result;
        }
      });
    return result;
  }

  getFirstCrumbUrl(sectionObject) {
    let result;
    let sectionUrl = !!sectionObject && sectionObject.id;
    let subSectionUrl = !!sectionObject && sectionObject.subSections[0].id;
    let topicUrl =
      !!sectionObject && !!sectionObject.subSections[0].topics
        ? sectionObject.subSections[0].topics[0].id
        : "";
    if (topicUrl !== "") {
      result = `/${sectionUrl}/${subSectionUrl}/${topicUrl}`;
    } else {
      result = `/${sectionUrl}/${subSectionUrl}`;
    }
    return result;
  }

  render() {
    const { copy } = this.props;
    const { params } = this.props.match;

    let arrayOfKeys = Object.keys(copy);
    let sectionObject;
    arrayOfKeys.forEach(key => {
      if (copy[key].id === params.sectionId) {
        return (sectionObject = copy[key]);
      }
    });

    let crumbs = [
      {
        title: !!sectionObject && sectionObject.shortHeading,
        url: this.getFirstCrumbUrl(sectionObject)
      },
      this.getSubSectionTitle(sectionObject)
    ];

    if (!!params.topicId) {
      crumbs.push(this.getTopicTitle(sectionObject));
    }

    let sectionColor = getSectionColor(
      this.props.match.params.sectionId,
      this.props.copy
    );

    return (
      <Container>
        <Breadcrumbs crumbs={crumbs} sectionColor={sectionColor} />
        <LongHeading>
          {!!sectionObject && sectionObject.longHeading}
        </LongHeading>
      </Container>
    );
  }
}

export default withRouter(HeaderContainer);

const Container = styled.div`
  max-width: 1200px;
  margin: 0 auto 2em auto;
`;

const LongHeading = styled.h1`
  text-transform: uppercase;
  color: ${props => props.theme.colorBlack};
  font-weight: 400;
  font-size: 24px;
  text-align: center;
  border-bottom: 2px solid ${props => props.theme.colorLightGrey};
  margin: 0
  padding: 0.5em;
`;
