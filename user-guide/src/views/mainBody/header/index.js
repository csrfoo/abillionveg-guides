import React, { Component } from "react";
import PropTypes from "prop-types";

import HeaderContainer from "./HeaderContainer.js";

class Header extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  render() {
    const { copy } = this.props;

    return <HeaderContainer copy={copy} />;
  }
}

export default Header;
