import React, { Component } from "react";
import { ThemeProvider } from "styled-components";
import "./App.css";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import theme from "./globalStyles.js";
import copy from "./constants/copy.js";
import {
  getWhatSubSections,
  getWhySubSections,
  getHowSubSections,
  getWhyTopics,
  getHowTopics
} from "./utils/helper.js";

import MobileNav from "./globalComponents/mobileNav";
import Layout from "./globalComponents/layout";
import NotFound from "./globalComponents/notFound";

import Landing from "./views/landing";

import WhatSection from "./views/mainBody/sections/what";
import WhySection from "./views/mainBody/sections/why";
import HowSection from "./views/mainBody/sections/how";

class App extends Component {
  render() {
    const copyCurrent = copy.en;
    return (
      <Router>
        <div className="app-container">
          <ThemeProvider theme={theme}>
            <Landing copy={copyCurrent} />
            <MobileNav copy={copyCurrent} />
            <Switch>
              <Route
                exact
                path={`/:sectionId(what)/:subSectionId(${getWhatSubSections(
                  copyCurrent
                )})`}
              >
                <Layout copy={copyCurrent}>
                  <Route
                    exact
                    path={`/what/:subSectionId`}
                    component={() => <WhatSection copy={copyCurrent} />}
                  />
                </Layout>
              </Route>
              <Route
                exact
                path={`/:sectionId(why)/:subSectionId(${getWhySubSections(
                  copyCurrent
                )})/:topicId(${getWhyTopics(copyCurrent)})`}
              >
                <Layout copy={copyCurrent}>
                  <Route
                    exact
                    path={`/why/:subSectionId/:topicId`}
                    component={() => <WhySection copy={copyCurrent} />}
                  />
                </Layout>
              </Route>
              <Route
                exact
                path={`/:sectionId(how)/:subSectionId(${getHowSubSections(
                  copyCurrent
                )})/:topicId(${getHowTopics(copyCurrent)})?`}
              >
                <Layout copy={copyCurrent}>
                  <Route
                    exact
                    path={`/how/:subSectionId/:topicId?`}
                    component={() => <HowSection copy={copyCurrent} />}
                  />
                </Layout>
              </Route>
              <Route component={NotFound} copy={copyCurrent} />
            </Switch>
          </ThemeProvider>
        </div>
      </Router>
    );
  }
}

export default App;
