const theme = {
  colorOrange: "#F59165",
  colorDarkOrange: "#FF6633",
  colorGreen: "#67C3A6",
  colorLightestGreen: "#E1F3ED",
  colorLightGreen: "#7ACAB1",
  colorDarkGreen: "#55BC9B",
  colorDarkerGreen: "#47B190",
  colorPink: "#F27C7C",
  colorDarkPink: "#F06565",
  colorPurple: "#B87DB5",
  colorLightPurple: "#FCFAF9",
  colorDarkPurple: "#Af6DAC",
  colorBlue: "#5B67F8",
  colorLightBlue: "#5B67F8BF",
  colorPinkWithOpacity: "rgba(242,124,124, 0.8)",
  colorPurpleWithOpacity: "rgba(184,125,181, 0.8)",
  colorGrey: "#9e9e9e",
  colorLightGrey: "#A9A9A9",
  colorLightestGrey: "#e8e8e8",
  colorBlack: "#353535",
  maxContentWidth: "1920px",
  modalStickyHeaderZIndex: 2
};

export default theme;
