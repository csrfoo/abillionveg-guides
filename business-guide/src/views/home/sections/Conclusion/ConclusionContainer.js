import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import Button from "../../../../globalComponents/button.js";

import theme from "../../../../globalStyles.js";

import Waiter2Png from "../../../../assets/waiter-2.png";

class ConclusionContainer extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  renderHeader() {
    const { copy } = this.props;
    return (
      <HeaderDiv>
        <ImageDivLeft>
          <Img src={Waiter2Png} alt="waiter-2" />
        </ImageDivLeft>
        <H1>{copy.conclusion}</H1>
        <ImageDivRight>
          <Img src={Waiter2Png} alt="waiter-2" />
        </ImageDivRight>
      </HeaderDiv>
    );
  }

  renderPara() {
    const { copy, externalLinks } = this.props;
    return (
      <ParaContainer>
        <p>{copy.conclusionPara}</p>

        <a
          href={externalLinks.claimYourBusiness}
          target="_blank"
          rel="noopener noreferrer"
        >
          <Button
            text={copy.claimYourBusiness}
            bgColor={theme.colorBlue}
            isUppercase
          />
        </a>
      </ParaContainer>
    );
  }

  render() {
    return (
      <BgWrapper>
        <Container>
          <Content>
            {this.renderHeader()}
            {this.renderPara()}
          </Content>
        </Container>
      </BgWrapper>
    );
  }
}

export default ConclusionContainer;

const BgWrapper = styled.div`
  background: ${props => props.theme.colorGreen};
`;

const Container = styled.div`
  max-width: ${props => props.theme.maxContentWidth};
  margin: 0 auto;
  // border-bottom: 2px solid white;
`;

const Content = styled.div`
  padding: 1.5em;
  @media (min-width: 600px) {
    width: 80%;
    margin: 0 auto;
    display: flex;
    padding: 3em;
    padding-bottom: 0;
  }
`;

const HeaderDiv = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  color: white;
  margin-bottom: 1em;
  @media (min-width: 600px) {
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start
    flex: 1;
  }
`;

const ImageDivLeft = styled.div`
  width: 25%;
  @media (min-width: 600px) {
    order: 2;
    width: 60%;
  }
`;

const ImageDivRight = styled.div`
  width: 25%;
  -moz-transform: scale(-1, 1);
  -webkit-transform: scale(-1, 1);
  -o-transform: scale(-1, 1);
  -ms-transform: scale(-1, 1);
  transform: scale(-1, 1);
  @media (min-width: 600px) {
    display: none;
  }
`;

const H1 = styled.h1`
  padding-left: 0.2em;
  padding-right: 0.2em;
  overflow: hidden;
  margin: 0;
  text-align: center;
  font-size: 20px;
  @media (min-width: 600px) {
    padding-left: 0;
    padding-right: 1em;

    font-size: 20px;
    text-align: left;
    margin-bottom: 1em;
  }
  @media (min-width: 800px) {
    font-size: 24px;
  }
  @media (min-width: 900px) {
    font-size: 28px;
  }
  @media (min-width: 1200px) {
    font-size: 36px;
  }
`;

const Img = styled.img`
  width: 100%;
  height: auto;
`;

const ParaContainer = styled.div`
  color: white;
  font-size: 16px;
  text-align: center;
  @media (min-width: 600px) {
    flex: 2;
    display: flex;
    flex-direction: column;
    text-align: left;
    font-size: 24px;
  }
  a {
    text-decoration: none;
  }
  p {
    margin-top: 0;
  }
`;
