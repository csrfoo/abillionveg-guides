import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import AccordionSection from "./AccordionSection";

class AccordionContainer extends Component {
  static propTypes = {
    children: PropTypes.instanceOf(Object).isRequired
  };

  constructor(props) {
    super(props);

    const openSections = {};
    this.state = { openSections };
  }

  handleOnClick(label) {
    const { openSections } = this.state;
    const isOpen = !!openSections[label];
    this.setState({
      openSections: {
        [label]: !isOpen
      }
    });
  }

  render() {
    const { children } = this.props;
    const { openSections } = this.state;

    return (
      <Container>
        {React.Children.map(children.props.children, (child, index) => (
          <AccordionSection
            isOpen={!!openSections[child.props.label]}
            label={child.props.label}
            onClick={label => this.handleOnClick(label)}
            isFirstSection={index === 0}
            key={index}
          >
            {child.props.children}
          </AccordionSection>
        ))}
      </Container>
    );
  }
}

export default AccordionContainer;

const Container = styled.div`
  width: 100%;
`;
