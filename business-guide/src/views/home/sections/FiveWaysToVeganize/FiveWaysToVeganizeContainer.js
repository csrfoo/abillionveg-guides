import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import AccordionContainer from "./local_components/Accordion/AccordionContainer.js";

class FiveWaysToVeganizeContainer extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired
  };

  renderContent() {
    const { copy } = this.props;

    const accordionContent = [
      {
        label: copy.veganizeMenu1Label,
        bulletPoints: [copy.veganizeMenu1Point1, copy.veganizeMenu1Point2]
      },
      {
        label: copy.veganizeMenu2Label,
        bulletPoints: [
          copy.veganizeMenu2Point1,
          copy.veganizeMenu2Point2,
          copy.veganizeMenu2Point3,
          copy.veganizeMenu2Point4
        ]
      },
      {
        label: copy.veganizeMenu3Label,
        bulletPoints: [
          copy.veganizeMenu3Point1,
          copy.veganizeMenu3Point2,
          copy.veganizeMenu3Point3,
          copy.veganizeMenu3Point4
        ]
      },
      {
        label: copy.veganizeMenu4Label,
        bulletPoints: [
          copy.veganizeMenu4Point1,
          copy.veganizeMenu4Point2,
          copy.veganizeMenu4Point3
        ]
      },
      {
        label: copy.veganizeMenu5Label,
        bulletPoints: [
          copy.veganizeMenu5Point1,
          copy.veganizeMenu5Point2,
          copy.veganizeMenu5Point3
        ]
      }
    ];

    return (
      <React.Fragment>
        {accordionContent.map((section, index) => (
          <div label={section.label} key={`section-${index}`}>
            {section.bulletPoints.map((item, index) => (
              <BulletPointDiv key={`bulletPoint-${index}`}>
                <BulletPoint />
                <p>{item}</p>
              </BulletPointDiv>
            ))}
          </div>
        ))}
      </React.Fragment>
    );
  }

  render() {
    const { copy } = this.props;

    return (
      <BgWrapper>
        <Container>
          <Content>
            <H1>{copy.fiveWaysToVeganizeYourMenu}</H1>
            <AccordionContainer>{this.renderContent()}</AccordionContainer>
          </Content>
        </Container>
      </BgWrapper>
    );
  }
}

export default FiveWaysToVeganizeContainer;

const BgWrapper = styled.div`
  background: ${props => props.theme.colorOrange};
`;

const Container = styled.div`
  max-width: ${props => props.theme.maxContentWidth};
  margin: 0 auto;

  color: white;
  padding: 2em 0;
  @media (min-width: 600px) {
    padding: 4em 0;
  }
`;

const Content = styled.div`
  width: 85%;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  @media (min-width: 600px) {
    flex-direction: row;
    width: 75%;
  }
`;

const H1 = styled.h1`
  width: 100%;
  text-align: center;
  margin-right: 0;
  font-weight: 800;
  margin-top: 0;
  margin-bottom: 1em;
  font-size: 20px;
  @media (min-width: 600px) {
    width: 25%;
    margin-right: 10%;
    text-align: left;
  }
  @media (min-width: 800px) {
    font-size: 24px;
  }
  @media (min-width: 900px) {
    font-size: 28px;
  }
  @media (min-width: 1200px) {
    font-size: 36px;
  }
`;

const BulletPointDiv = styled.div`
  margin-bottom: 1em;
  p {
    display: inline;
    font-size: 16px @media (min-width: 600px) {
      font-size: 18px;
    }
  }
`;

const BulletPoint = styled.span`
  display: inline-block;
  height: 10px;
  width: 10px;
  margin-right: 2em;
  background-color: white;
  transform: rotate(45deg);
`;
