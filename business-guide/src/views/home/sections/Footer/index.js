import React, { Component } from "react";
import PropTypes from "prop-types";

import FooterContainer from "./FooterContainer.js";

class FooterSection extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired,
    externalLinks: PropTypes.object.isRequired
  };

  render() {
    const { copy, externalLinks } = this.props;
    return <FooterContainer copy={copy} externalLinks={externalLinks} />;
  }
}

export default FooterSection;
