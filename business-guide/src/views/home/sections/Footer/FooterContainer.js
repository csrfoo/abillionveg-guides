import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  EmailShareButton
} from "react-share";

class FooterContainer extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired,
    externalLinks: PropTypes.object.isRequired
  };

  render() {
    const { copy, externalLinks } = this.props;

    const socials = [
      {
        iconClass: "fab fa-lg fa-facebook-square",
        shareButton: FacebookShareButton
      },
      {
        iconClass: "fab fa-lg fa-linkedin",
        shareButton: LinkedinShareButton
      },
      {
        iconClass: "fab fa-lg fa-twitter-square",
        shareButton: TwitterShareButton
      },
      {
        iconClass: "fas fa-lg fa-envelope-square",
        shareButton: EmailShareButton
      }
    ];

    return (
      <BgWrapper>
        <Container>
          <Content>
            <P>{copy.shareThisGuide}</P>
            <SocialsDiv>
              {socials.map((social, index) => (
                <ShareButtonDiv key={index}>
                  <social.shareButton
                    url={externalLinks.businessGuideShareLink}
                  >
                    <ImgDiv>
                      <i className={social.iconClass} />
                    </ImgDiv>
                  </social.shareButton>
                </ShareButtonDiv>
              ))}
            </SocialsDiv>
          </Content>
        </Container>
      </BgWrapper>
    );
  }
}

export default FooterContainer;

const BgWrapper = styled.div`
  background: ${props => props.theme.colorGreen};
  padding-bottom: 2em;
`;

const Container = styled.div`
  max-width: ${props => props.theme.maxContentWidth};
  margin: 0 auto;
`;

const P = styled.div`
  font-size: 12px;
  @media (min-width: 600px) {
    margin-bottom: 0;
    margin-right: 0.5em;
    font-size: 20px;
  }
  @media (min-width: 1200px) {
    font-size: 24px;
  }
`;

const Content = styled.div`
  font-size: 16px;
  color: white;
  text-transform: uppercase;
  display: flex;
  width: 85%;
  margin: 0 auto;
  display: flex;
  padding: 0.5em 0;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  @media(min-width: 600px){
    text-align: right;
    font-size: 24px
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;


  }
`;

const SocialsDiv = styled.div`
  display: flex;
  align-items: center;
  margin-top: 1em;
  @media (min-width: 600px) {
    margin-top: 0;
  }
`;

const ShareButtonDiv = styled.div`
  cursor: pointer;
  padding: 0.5em 0.5em;
  div {
    &:focus {
      outline-width: 0px !important;
      outline: none !important;
    }
  }
  @media (min-width: 600px) {
    padding: 0 8px;
  }
`;

const ImgDiv = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;
