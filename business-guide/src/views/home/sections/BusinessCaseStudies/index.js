import React, { Component } from "react";
import PropTypes from "prop-types";

import BusinessCaseStudiesContainer from "./BusinessCaseStudiesContainer.js";

class BusinessCaseStudiesSection extends Component {
  static propTypes = {
    copy: PropTypes.object.isRequired,
    businessNames: PropTypes.object.isRequired
  };

  render() {
    const { copy, businessNames } = this.props;
    return (
      <BusinessCaseStudiesContainer copy={copy} businessNames={businessNames} />
    );
  }
}

export default BusinessCaseStudiesSection;
