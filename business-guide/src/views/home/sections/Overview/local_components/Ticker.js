import React, { Component } from "react";
import Ticker from "react-ticker";
import styled from "styled-components";

class TickerCustom extends Component {
  render() {
    const { copy } = this.props;
    return (
      <Ticker speed={8}>
        {({ index }) => (
          <React.Fragment>
            <Uppercase>{copy.tickerBreakingNews}</Uppercase>
            <SentenceCase>{copy.tickerVegans}</SentenceCase>
          </React.Fragment>
        )}
      </Ticker>
    );
  }
}

export default TickerCustom;

const Uppercase = styled.p`
  display: inline-block;
  color: white;
  text-transform: uppercase;
  padding: 0em 1em;
  font-size: 12px;
  @media (min-width: 900px) {
    font-size: 24px;
  }
`;

const SentenceCase = styled.p`
  display: inline-block;
  color: white;
  font-weight: bold;
  font-size: 12px;
  padding: 0em 1em;
  @media (min-width: 900px) {
    font-size: 24px;
  }
`;
