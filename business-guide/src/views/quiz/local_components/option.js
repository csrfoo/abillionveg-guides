import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

class Option extends Component {
  static propTypes = {
    option: PropTypes.object.isRequired,
    isSelected: PropTypes.bool
  };

  handleOnClick(option, sectionTitle) {
    this.props.onClick(option, sectionTitle);
  }

  render() {
    const { option, sectionTitle, isSelected } = this.props;
    return (
      <Container
        onClick={() => this.handleOnClick(option.name, sectionTitle)}
        isSelected={isSelected}
      >
        <Circle isSelected={isSelected}>{isSelected && <InnerCircle />}</Circle>
        <P>{option.name}</P>
      </Container>
    );
  }
}

export default Option;

const Container = styled.div`
  border: 2px solid ${props => props.theme.colorGreen};
  background: ${props => props.isSelected && props.theme.colorGreen}
  color: ${props => (props.isSelected ? "white" : "black")}
  padding: 0.3em;
  margin-bottom: 0.5em;
  display: flex;
  align-items: center;
  p {
    margin: 0;
  }
  cursor: pointer;
  @media(min-width: 600px){
    margin-bottom: 1em;
    padding: 0.5em;
  }
`;

const Circle = styled.div`
  position: relative;
  width: 20px;
  height: 20px;
  box-sizing: border-box;
  border-radius: 20px;
  border: ${props =>
    props.isSelected
      ? "4px solid white"
      : `2px solid ${props.theme.colorGreen}`}
  margin-right: 0.5em;
  @media(min-width: 600px){
    margin-right: 1em;
  }
`;

const InnerCircle = styled.div`
  position: absolute;
  background: ${props => props.theme.colorGreen};
`;

const P = styled.p`
  flex: 2;
`;
