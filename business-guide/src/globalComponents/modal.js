import React, { Component } from "react";
import styled from "styled-components";

import Breadcrumbs from "./breadcrumbs.js";

class Modal extends Component {
  constructor(props) {
    super(props);
    this.modal = React.createRef();
  }
  componentDidUpdate(prevProps) {
    !!this.modal && !!this.modal.current && this.modal.current.scrollTo(0, 0);

    if (prevProps.isVisible !== this.props.isVisible) {
      if (document.body.style.overflow === "") {
        document.body.style.overflow = "hidden";
      } else {
        document.body.style.overflow = "";
      }
    }
  }

  render() {
    const {
      handleClose,
      isVisible,
      children,
      breadcrumbItems,
      moreStyles,
      title
    } = this.props;

    return (
      <React.Fragment>
        <DarkLayover isVisible={isVisible} onClick={handleClose} />
        <ModalMain isVisible={isVisible} moreStyles={moreStyles}>
          <ContentContainer>
            <ModalHeader>
              <ModalHeaderContainer>
                {!!breadcrumbItems && (
                  <Breadcrumbs breadcrumbItems={breadcrumbItems} />
                )}
                {!!title && <Title color={moreStyles.color}>{title}</Title>}
                <CloseSpan onClick={handleClose}>
                  <p>X</p>
                </CloseSpan>
              </ModalHeaderContainer>
            </ModalHeader>
            <Content ref={this.modal}>
              <ContentDiv>{children}</ContentDiv>
            </Content>
          </ContentContainer>
        </ModalMain>
      </React.Fragment>
    );
  }
}

export default Modal;

const DarkLayover = styled.div`
  display: ${props => (props.isVisible ? "block" : "none")};
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.6);
  z-index: 1;
`;

const ModalMain = styled.div`
  z-index: ${props => props.theme.modalStickyHeaderZIndex};
  border: ${props => (props.moreStyles ? props.moreStyles.border : "")};
  color: ${props => (props.moreStyles ? props.moreStyles.color : "")};
  display: ${props => (props.isVisible ? "block" : "none")};
  position: fixed;
  background: white;
  width: 90%;
  height: 90vh;

  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  animation-name: animateSlideInFromRight;
  animation-duration: 0.4s;
  @media (min-width: 600px) {
    width: 600px;
  }
  @keyframes animateSlideInFromRight {
    from {
      left: 2000px;
    }
    to {
      left: 50%;
    }
  }
  p {
    font-size: 18px;
  }
  @media (min-width: 600px) {
    overflow: hidden;
  }
`;

const ContentContainer = styled.div`
  // height: 90vh;
  height: 100%;
`;

const Content = styled.div`
  padding: 0 1em;
  height: calc(100%-100px);
  max-height: 100%
  overflow: auto;

  @media (min-width: 600px) {
    padding: 1.5em 3em;
  }

`;

const ContentDiv = styled.div`
  margin-bottom: 100px;
  @media (min-width: 600px) {
    margin-bottom: 100px;
  }
`;

const ModalHeader = styled.div`
  z-index: 2
  display: flex;
  position: fixed
  background: white;
  width: 100%
  justify-content: space-between;
`;

const ModalHeaderContainer = styled.div`
  display: flex;

  padding: 1em 1em 0em 1em;
  width: 100%;
  justify-content: space-between;
  @media (min-width: 600px) {
    padding: 0.5em 3em 0em 3em;
  }
`;

const Title = styled.h1`
  color: ${props => props.color}
  font-size: 24px;
  padding-right: 20px;
`;

const CloseSpan = styled.div`
  cursor: pointer;
  display: inline-block;
  font-weight: 100;
  font-size: 40px;
  &:hover {
    font-weight: 200;
  }
  p{
    font-weight: 100;
    font-size: 40px
    margin-top: 0
    margin-bottom: 0

  }
`;
