const businessNames = {
  CHIPOTLE: "Chipotle",
  PIZZAEXPRESS: "Pizza Express",
  PRET: "Pret a Manger",
  EMPRESS: "Empress",
  CAFES: "Cafes"
};

export default businessNames;
