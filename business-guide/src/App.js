import React from "react";
import { ThemeProvider } from "styled-components";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";

import HeroSection from "./views/home/sections/Hero";
import OverviewSection from "./views/home/sections/Overview";
import WhatIsVeganFoodSection from "./views/home/sections/WhatIsVeganFood";
import WhyShouldIVeganizeSection from "./views/home/sections/WhyShouldIVeganize";
import FiveWaysToVeganizeSection from "./views/home/sections/FiveWaysToVeganize";
import BusinessCaseStudiesSection from "./views/home/sections/BusinessCaseStudies";
// import GetStartedYourselfSection from "./views/home/sections/GetStartedYourself";
import ConclusionSection from "./views/home/sections/Conclusion";
import FooterSection from "./views/home/sections/Footer";

import Quiz from "./views/quiz";

import theme from "./globalStyles.js";
import copy from "./constants/copy.js";
import businessNames from "./constants/businessNames.js";
import externalLinks from "./constants/externalLinks.js";
import routes from "./constants/routes.js";

function App() {
  const copyCurrent = copy.en;

  return (
    <div className="app-container">
      <ThemeProvider theme={theme}>
        <Router>
          <Switch>
            <Route path={routes.quiz}>
              <Quiz
                copy={copyCurrent}
                externalLinks={externalLinks}
                routes={routes}
              />
            </Route>

            <Route path={routes.home}>
              <HeroSection />
              <OverviewSection copy={copyCurrent} />
              <WhatIsVeganFoodSection copy={copyCurrent} />
              <WhyShouldIVeganizeSection copy={copyCurrent} routes={routes} />
              <FiveWaysToVeganizeSection copy={copyCurrent} />
              <BusinessCaseStudiesSection
                copy={copyCurrent}
                businessNames={businessNames}
              />
              {/*<GetStartedYourselfSection copy={copyCurrent} />*/}
              <ConclusionSection
                copy={copyCurrent}
                externalLinks={externalLinks}
              />
              <FooterSection copy={copyCurrent} externalLinks={externalLinks} />
            </Route>
          </Switch>
        </Router>
      </ThemeProvider>
    </div>
  );
}

export default App;
